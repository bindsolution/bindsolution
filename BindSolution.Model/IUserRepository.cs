﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="IUserRepository.cs" company="BindSolution">
//      All rights reserved BindSolution.Data
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using BindSolution.Framework.Domain;

namespace BindSolution.Data
{
    public interface IUserRepository : IRepository<User>
    {

    }
}