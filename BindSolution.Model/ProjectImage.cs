#region copyright
// -----------------------------------------------------------------------
//  <copyright file="ProjectImage.cs" company="BindSolution">
//      All rights reserved BindSolution
//  </copyright>
// -----------------------------------------------------------------------
#endregion

namespace BindSolution.Data
{
    using System;
    using Framework.Domain;

    public class ProjectImage : IAuditable
    {
        public Guid ID { get; private set; }
        public string Name { get; set; }

        #region Implementation of IAuditable

        public DateTime CreatedAt { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public string UpdatedBy { get; set; }

        #endregion

        public ProjectImage()
        {
            ID = Guid.NewGuid();
        }
    }
}