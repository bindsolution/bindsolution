﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="Project.cs" company="BindSolution">
//      All rights reserved BindSolution
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using BindSolution.Framework.Domain;

namespace BindSolution.Data
{
    #region usings

    using System;
    using System.Collections.Generic;

    #endregion

    public class Project : IAuditable
    {
        public Guid ID { get; private set; }
        public string Name { get; set; }
        public string WebSite { get; set; }
        public string Keyword { get; set; }
        public string Details { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<ProjectImage> Images { get; set; }
        public string Logo { get; set; }
        public short Rate { get; set; }
        public bool Published { get; set; }

        #region Implementation of IAuditable

        public DateTime CreatedAt { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public string UpdatedBy { get; set; }

        #endregion

        public Project()
        {
            ID = Guid.NewGuid();
        }
    }
}
