﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="RoleRepository.cs" company="BindSolution">
//      All rights reserved BindSolution.Data
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using BindSolution.Framework.Domain;

namespace BindSolution.Data
{
    public class RoleRepository : IRoleRepository
    {
        private readonly BindSolutionContext _context;

        public RoleRepository(BindSolutionContext context)
        {
            _context = context;
        }

        #region Implementation of IRepository<Role>

        public Role Get(Guid id)
        {
            return _context.Roles.Where(u => u.ID == id).SingleOrDefault();
        }

        public void Add(Role entity)
        {
            _context.Roles.Add(entity);
        }

        public void Attach(Role entity)
        {
            _context.Roles.Attach(entity);
        }

        public void Remove(Role entity)
        {
            _context.Roles.Remove(entity);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public IQueryable<Role> Query(params Expression<Func<Role, object>>[] includeProperties)
        {
            return includeProperties.Aggregate<Expression<Func<Role, object>>,
                IQueryable<Role>>(_context.Roles, (current, includeProperty) => current.Include(includeProperty));
        }

        public IQueryable<Role> Query(int pageIndex, int pageSize, params Expression<Func<Role, object>>[] includeProperties)
        {
            return includeProperties.Aggregate<Expression<Func<Role, object>>,
                IQueryable<Role>>(_context.Roles, (current, includeProperty) => current.Include(includeProperty)).Skip(pageIndex).Take(pageSize);
        }

        #endregion
    }
}