﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="RoleRepository.cs" company="BindSolution">
//      All rights reserved BindSolution.Data
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BindSolution.Data.Infra;
using BindSolution.Framework.Domain;

namespace BindSolution.Data.Fake
{
    public class RoleRepository : IRoleRepository
    {
        private readonly HashSet<Role> _context = new HashSet<Role>();

        public RoleRepository()
        {
            _context.Add(new Role { Name = RoleConstants.Administrators });
            Save();
        }

        #region Implementation of IRepository<Role>

        public Role Get(Guid id)
        {
            return _context.Where(u => u.ID == id).SingleOrDefault();
        }

        public void Add(Role entity)
        {
            _context.Add(entity);
        }

        public void Attach(Role entity)
        {
            var pjt = Get(entity.ID);
            Remove(pjt);
            Add(entity);
        }

        public void Remove(Role entity)
        {
            _context.Remove(entity);
        }

        public int Save()
        {
            return 1;
        }

        public IQueryable<Role> Query(params Expression<Func<Role, object>>[] includeProperties)
        {
            return _context.AsQueryable();
        }

        public IQueryable<Role> Query(int pageIndex, int pageSize, params Expression<Func<Role, object>>[] includeProperties)
        {
            return _context.Skip(pageIndex).Take(pageSize).AsQueryable();
        }

        #endregion
    }
}