#region copyright
// -----------------------------------------------------------------------
//  <copyright file="ProjectRepository.cs" company="BindSolution">
//      All rights reserved BindSolution.Test
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BindSolution.Data.Fake
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly HashSet<Project> _context = new HashSet<Project>();

        #region Implementation of IRepository<Project>

        public Project Get(Guid id)
        {
            return _context.Where(u => u.ID == id).SingleOrDefault();
        }

        public void Add(Project entity)
        {
            entity.Images = new List<ProjectImage>();
            entity.Addresses = new List<Address>();
            _context.Add(entity);
        }

        public void Attach(Project entity)
        {
            var pjt = Get(entity.ID);
            Remove(pjt);
            Add(entity);
        }

        public void Remove(Project entity)
        {
            _context.Remove(entity);
        }

        public int Save()
        {
            return 1;
        }

        public IQueryable<Project> Query(params Expression<Func<Project, object>>[] includeProperties)
        {
            return _context.AsQueryable();
        }

        public IQueryable<Project> Query(int pageIndex, int pageSize, params Expression<Func<Project, object>>[] includeProperties)
        {
            return _context.Skip(pageIndex).Take(pageSize).AsQueryable();
        }

        #endregion
    }
}