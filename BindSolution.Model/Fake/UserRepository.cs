﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="UserRepository.cs" company="BindSolution">
//      All rights reserved BindSolution.Data
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BindSolution.Data.Infra;
using BindSolution.Framework;
using BindSolution.Framework.Domain;

namespace BindSolution.Data.Fake
{
    public class UserRepository : IUserRepository
    {
        private readonly IRoleRepository _roleRepository;
        private readonly HashSet<User> _context = new HashSet<User>();

        public UserRepository(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;

            var role = (from r in _roleRepository.Query()
                        where r.Name == RoleConstants.Administrators
                        select r).FirstOrDefault();

            var user = new User
            {
                Email = "contato@bindsolution.com"
                ,
                Password = "$0l@res".Sha1Hash().ToLower()
                ,
                Name = "Admin"
                ,
                IsBlock = false
                ,
                IsApproved = true
                ,
                Roles = new List<Role> { role }
            };

            _context.Add(user);
            Save();
        }


        #region Implementation of IRepository<User>

        public User Get(Guid id)
        {
            return _context.Where(u => u.ID == id).SingleOrDefault();
        }

        public void Add(User entity)
        {
            _context.Add(entity);
        }

        public void Attach(User entity)
        {
            var pjt = Get(entity.ID);
            Remove(pjt);
            Add(entity);
        }

        public void Remove(User entity)
        {
            _context.Remove(entity);
        }

        public int Save()
        {
            return 1;
        }

        public IQueryable<User> Query(params Expression<Func<User, object>>[] includeProperties)
        {
            return _context.AsQueryable();
        }

        public IQueryable<User> Query(int pageIndex, int pageSize, params Expression<Func<User, object>>[] includeProperties)
        {
            return _context.Skip(pageIndex).Take(pageSize).AsQueryable();
        }

        #endregion
    }
}