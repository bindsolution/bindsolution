﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="RoleConstants.cs" company="BindSolution">
//      All rights reserved BindSolution.Data
//  </copyright>
// -----------------------------------------------------------------------
#endregion

namespace BindSolution.Data.Infra
{
    public class RoleConstants
    {
        public const string Administrators = "administrators";
        public const string Managers = "administrators, managers";
    }
}