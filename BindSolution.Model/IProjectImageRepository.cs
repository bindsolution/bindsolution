﻿// //-----------------------------------------------------------------------
// // <copyright file="IProjectImageRepository.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

using BindSolution.Framework.Domain;

namespace BindSolution.Data
{
    public interface IProjectImageRepository : IRepository<ProjectImage>
    {

    }
}