﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="DatabaseInitializer.cs" company="BindSolution">
//      All rights reserved BindSolution.Data
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using System.Collections.Generic;
using System.Data.Entity;
using BindSolution.Data.Infra;
using BindSolution.Framework;
using BindSolution.Framework.Domain;

namespace BindSolution.Data
{
    public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<BindSolutionContext>
    {
        protected override void Seed(BindSolutionContext context)
        {
            var role = new Role
                           {
                               Name = RoleConstants.Administrators
                           };

            var user = new User
                           {
                               Email = "contato@bindsolution.com"
                               ,
                               Password = "$0l@res".Sha1Hash().ToLower()
                               ,
                               Name = "Admin"
                               ,
                               IsBlock = false
                               ,
                               IsApproved = true
                               ,
                               Roles = new List<Role> { role }
                           };

            context.Users.Add(user);
            context.SaveChanges();

            base.Seed(context);
        }
    }
}