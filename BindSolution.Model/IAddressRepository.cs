using BindSolution.Framework.Domain;

namespace BindSolution.Data
{
    public interface IAddressRepository : IRepository<Address>
    {
    }
}