﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="IRoleRepository.cs" company="BindSolution">
//      All rights reserved BindSolution.Data
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using BindSolution.Framework.Domain;

namespace BindSolution.Data
{
    public interface IRoleRepository : IRepository<Role>
    {

    }
}