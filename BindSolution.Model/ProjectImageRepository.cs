﻿// //-----------------------------------------------------------------------
// // <copyright file="ProjectImageRepository.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace BindSolution.Data
{
    public class ProjectImageRepository : IProjectImageRepository
    {
        private readonly BindSolutionContext _context;

        public ProjectImageRepository(BindSolutionContext context)
        {
            _context = context;
        }

        #region Implementation of IRepository<ProjectImage>

        public ProjectImage Get(Guid id)
        {
            return _context.ProjectImages.Where(u => u.ID == id).SingleOrDefault();
        }

        public void Add(ProjectImage entity)
        {
            _context.ProjectImages.Add(entity);
        }

        public void Attach(ProjectImage entity)
        {
            _context.ProjectImages.Attach(entity);
        }

        public void Remove(ProjectImage entity)
        {
            _context.ProjectImages.Remove(entity);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public IQueryable<ProjectImage> Query(params Expression<Func<ProjectImage, object>>[] includeProperties)
        {
            return includeProperties.Aggregate<Expression<Func<ProjectImage, object>>,
                IQueryable<ProjectImage>>(_context.ProjectImages, (current, includeProperty) => current.Include(includeProperty));
        }

        public IQueryable<ProjectImage> Query(int pageIndex, int pageSize, params Expression<Func<ProjectImage, object>>[] includeProperties)
        {
            return includeProperties.Aggregate<Expression<Func<ProjectImage, object>>,
                IQueryable<ProjectImage>>(_context.ProjectImages, (current, includeProperty) => current.Include(includeProperty)).OrderBy(p => p.Name).Skip(pageIndex).Take(pageSize);
        }

        #endregion
    }
}