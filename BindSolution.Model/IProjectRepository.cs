﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="IProjectRepository.cs" company="BindSolution">
//      All rights reserved BindSolution.Data
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using BindSolution.Framework.Domain;

namespace BindSolution.Data
{
    public interface IProjectRepository : IRepository<Project>
    {

    }
}