﻿// //-----------------------------------------------------------------------
// // <copyright file="AddressRepository.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace BindSolution.Data
{
    public class AddressRepository : IAddressRepository
    {
        private readonly BindSolutionContext _context;

        public AddressRepository(BindSolutionContext context)
        {
            _context = context;
        }

        #region Implementation of IRepository<Project>

        public Address Get(Guid id)
        {
            return _context.Addresses.Where(u => u.ID == id).SingleOrDefault();
        }

        public void Add(Address entity)
        {
            _context.Addresses.Add(entity);
        }

        public void Attach(Address entity)
        {
            _context.Addresses.Attach(entity);
        }

        public void Remove(Address entity)
        {
            _context.Addresses.Remove(entity);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public IQueryable<Address> Query(params Expression<Func<Address, object>>[] includeProperties)
        {
            return includeProperties.Aggregate<Expression<Func<Address, object>>,
                IQueryable<Address>>(_context.Addresses, (current, includeProperty) => current.Include(includeProperty));
        }

        public IQueryable<Address> Query(int pageIndex, int pageSize, params Expression<Func<Address, object>>[] includeProperties)
        {
            return includeProperties.Aggregate<Expression<Func<Address, object>>,
                IQueryable<Address>>(_context.Addresses, (current, includeProperty) => current.Include(includeProperty)).OrderBy(p => p.Display).Skip(pageIndex).Take(pageSize);
        }

        #endregion
    }
}