﻿using System.Security.Principal;
using BindSolution.Data.Configuration;

namespace BindSolution.Data
{
    #region usings

    using System;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using Framework.Domain;

    #endregion

    public class BindSolutionContext : DbContext
    {
        private readonly Func<IIdentity> _getCurrentUser;
        public DbSet<Project> Projects { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<ProjectImage> ProjectImages { get; set; }

        public BindSolutionContext(Func<IIdentity> getCurrentUser)
            : base("name=Data")
        {
            _getCurrentUser = getCurrentUser;
            Database.SetInitializer(new DatabaseInitializer());
        }

        public override int SaveChanges()
        {
            UpdateModifiedEntries();
            UpdateAddedEntries();
            return base.SaveChanges();

        }

        private void UpdateAddedEntries()
        {
            var addedEntries = ChangeTracker.Entries().Where(e => e.State == EntityState.Added && e.Entity is IAuditable).Select(e => (IAuditable)e.Entity);
            var identity = _getCurrentUser();
            foreach (var addedEntry in addedEntries)
            {
                addedEntry.CreatedAt = DateTime.UtcNow;
                if (identity.IsAuthenticated)
                    addedEntry.CreatedBy = identity.Name;
            }
        }

        private void UpdateModifiedEntries()
        {
            var modifiedEntries = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified && e.Entity is IAuditable).Select(e => (IAuditable)e.Entity);
            var identity = _getCurrentUser();
            foreach (var modifiedEntry in modifiedEntries)
            {
                modifiedEntry.UpdatedAt = DateTime.UtcNow;
                if (identity.IsAuthenticated)
                    modifiedEntry.UpdatedBy = identity.Name;
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new ProjectConfiguration());
            modelBuilder.Configurations.Add(new ProjectImageConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new AddressConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
