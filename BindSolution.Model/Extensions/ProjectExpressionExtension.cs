﻿// //-----------------------------------------------------------------------
// // <copyright file="ProjectExpressionExtension.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

using System.Linq;

namespace BindSolution.Data.Extensions
{
    public static class ProjectExpressionExtension
    {
        public static IQueryable<Project> WithDefaultOrdered(this IQueryable<Project> list)
        {
            return list.OrderByDescending(p => p.Rate)
                .ThenByDescending(p => p.CreatedAt);
        }

        public static IQueryable<Project> OnlyPublishedProjects(this IQueryable<Project> list)
        {
            return list.Where(p => p.Published);
        }

        public static IQueryable<Project> OnlyDraftProjects(this IQueryable<Project> list)
        {
            return list.Where(p => !p.Published);
        }


    }
}