﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="UserRepository.cs" company="BindSolution">
//      All rights reserved BindSolution.Data
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using BindSolution.Framework.Domain;

namespace BindSolution.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly BindSolutionContext _context;

        public UserRepository(BindSolutionContext context)
        {
            _context = context;
        }

        #region Implementation of IRepository<User>

        public User Get(Guid id)
        {
            return _context.Users.Where(u => u.ID == id).SingleOrDefault();
        }

        public void Add(User entity)
        {
            _context.Users.Add(entity);
        }

        public void Attach(User entity)
        {
            _context.Users.Attach(entity);
        }

        public void Remove(User entity)
        {
            _context.Users.Remove(entity);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public IQueryable<User> Query(params Expression<Func<User, object>>[] includeProperties)
        {
            return includeProperties.Aggregate<Expression<Func<User, object>>,
                IQueryable<User>>(_context.Users, (current, includeProperty) => current.Include(includeProperty));
        }

        public IQueryable<User> Query(int pageIndex, int pageSize, params Expression<Func<User, object>>[] includeProperties)
        {
            return includeProperties.Aggregate<Expression<Func<User, object>>,
                IQueryable<User>>(_context.Users, (current, includeProperty) => current.Include(includeProperty)).Skip(pageIndex).Take(pageSize);
        }

        #endregion
    }
}