using System;

namespace BindSolution.Data.Exeptions
{
    public class ProjectAlreadyExistException : ApplicationException
    {
        public Project ExistProject { get;private set;}

        public ProjectAlreadyExistException(Project existProject)
        {
            ExistProject = existProject;
        }
    }
}