﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="ProjectRepository.cs" company="BindSolution">
//      All rights reserved BindSolution.Data
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace BindSolution.Data
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly BindSolutionContext _context;

        public ProjectRepository(BindSolutionContext context)
        {
            _context = context;
        }

        #region Implementation of IRepository<Project>

        public Project Get(Guid id)
        {
            return _context.Projects.Where(u => u.ID == id).SingleOrDefault();
        }

        public void Add(Project entity)
        {
            _context.Projects.Add(entity);
        }

        public void Attach(Project entity)
        {
            _context.Projects.Attach(entity);
        }

        public void Remove(Project entity)
        {
            _context.Projects.Remove(entity);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public IQueryable<Project> Query(params Expression<Func<Project, object>>[] includeProperties)
        {
            return includeProperties.Aggregate<Expression<Func<Project, object>>,
                IQueryable<Project>>(_context.Projects, (current, includeProperty) => current.Include(includeProperty));
        }

        public IQueryable<Project> Query(int pageIndex, int pageSize, params Expression<Func<Project, object>>[] includeProperties)
        {
            return includeProperties.Aggregate<Expression<Func<Project, object>>,
                IQueryable<Project>>(_context.Projects, (current, includeProperty) => current.Include(includeProperty)).OrderBy(p => p.Name).Skip(pageIndex).Take(pageSize);
        }

        #endregion
    }
}