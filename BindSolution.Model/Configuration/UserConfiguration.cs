// //-----------------------------------------------------------------------
// // <copyright file="UserConfiguration.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Data.Configuration
{
    #region usings

    using System.Data.Entity.ModelConfiguration;
    using Framework.Domain;

    #endregion

    /// <summary>
    /// Provides configuration database
    /// </summary>
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserConfiguration"/> class.
        /// </summary>
        public UserConfiguration()
        {
            HasKey(p => p.ID)
                .Property(p => p.ID)
                .IsRequired();

            Property(p => p.Name)
                .HasMaxLength(60)
                .IsRequired();

            Property(p => p.Email)
                .HasMaxLength(120)
                .IsRequired();

            Property(p => p.Password)
                .HasMaxLength(60);

            HasMany(r => r.Roles).WithMany();
        }
    }
}