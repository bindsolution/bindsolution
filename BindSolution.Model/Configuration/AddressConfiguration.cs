// //-----------------------------------------------------------------------
// // <copyright file="AddressConfiguration.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Data.Configuration
{
    #region usings

    using System.Data.Entity.ModelConfiguration;

    #endregion

    /// <summary>
    /// Provides configuration database
    /// </summary>
    public class AddressConfiguration : EntityTypeConfiguration<Address>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddressConfiguration"/> class.
        /// </summary>
        public AddressConfiguration()
        {
            HasKey(p => p.ID)
                .Property(p => p.ID)
                .IsRequired();

            Property(p => p.Display)
                .HasMaxLength(60)
                .IsRequired();

            Property(p => p.Street)
                .HasMaxLength(120)
                .IsRequired();

            Property(p => p.Number)
                .HasMaxLength(35)
                .IsRequired();

            Property(p => p.Complement)
                .HasMaxLength(35);

            Property(p => p.Latitude)
                .IsRequired();
            Property(p => p.Longitude)
                .IsRequired();

            Property(p => p.City)
                .HasMaxLength(120)
                .IsRequired();

            Property(p => p.State)
                .HasMaxLength(60)
                .IsRequired();

            Property(p => p.CreatedBy)
                .HasMaxLength(120)
                .IsRequired();
            Property(p => p.UpdatedBy)
                .HasMaxLength(120);
            Property(p => p.CreatedAt)
                .IsRequired();
        }
    }
}