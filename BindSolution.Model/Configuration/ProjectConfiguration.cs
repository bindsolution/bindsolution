// //-----------------------------------------------------------------------
// // <copyright file="ProjectConfiguration.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Data.Configuration
{
    #region usings

    using System.Data.Entity.ModelConfiguration;

    #endregion

    /// <summary>
    /// Provides configuration database
    /// </summary>
    public class ProjectConfiguration : EntityTypeConfiguration<Project>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectConfiguration"/> class.
        /// </summary>
        public ProjectConfiguration()
        {
            HasKey(p => p.ID)
                .Property(p => p.ID)
                .IsRequired();

            Property(p => p.Name)
                .HasMaxLength(60)
                .IsRequired();

            Property(p => p.WebSite)
                .HasMaxLength(180)
                .IsRequired();

            Property(p => p.Keyword)
                .HasMaxLength(35)
                .IsRequired();

            Property(p => p.Rate)
                .IsRequired();

            Property(p => p.Description)
                .HasMaxLength(255)
                .IsRequired();

            Property(p => p.Details)
                .IsRequired();

            Property(p => p.Logo)
                .HasMaxLength(255)
                .IsRequired();

            Property(p => p.CreatedBy)
                .HasMaxLength(120)
                .IsRequired();
            Property(p => p.UpdatedBy)
                .HasMaxLength(120);
            Property(p => p.CreatedAt)
                .IsRequired();

            // HasRequired(p => p.Images).WithMany();
            HasMany(p => p.Images).WithRequired().WillCascadeOnDelete();

            // FK in Address table (ProjectID NotNull)
            HasMany(p => p.Addresses).WithRequired().WillCascadeOnDelete();
        }
    }
}