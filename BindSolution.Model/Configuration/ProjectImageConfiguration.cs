// //-----------------------------------------------------------------------
// // <copyright file="ProjectImageConfiguration.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Data.Configuration
{
    #region usings

    using System.Data.Entity.ModelConfiguration;

    #endregion

    /// <summary>
    /// Provides configuration database
    /// </summary>
    public class ProjectImageConfiguration : EntityTypeConfiguration<ProjectImage>
    {
        public ProjectImageConfiguration()
        {
            HasKey(p => p.ID)
                .Property(p => p.ID);
            Property(p => p.Name)
                .HasMaxLength(180)
                .IsRequired();

            Property(p => p.CreatedBy)
                .HasMaxLength(120)
                .IsRequired();
            Property(p => p.UpdatedBy)
                .HasMaxLength(120);
            Property(p => p.CreatedAt)
                .IsRequired();
        }
    }
}