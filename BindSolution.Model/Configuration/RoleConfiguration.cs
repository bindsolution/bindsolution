// //-----------------------------------------------------------------------
// // <copyright file="RoleConfiguration.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Data.Configuration
{
    #region usings

    using System.Data.Entity.ModelConfiguration;
    using Framework.Domain;

    #endregion

    /// <summary>
    /// Provides configuration database
    /// </summary>
    public class RoleConfiguration : EntityTypeConfiguration<Role>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoleConfiguration"/> class.
        /// </summary>
        public RoleConfiguration()
        {
            HasKey(p => p.ID)
                .Property(p => p.ID);

            Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(70);
        }
    }
}