using System;
using System.Collections.Generic;
using System.IO;

namespace BindSolution.Data
{
    public interface IProjectImageWrap
    {
        bool Save(Guid projectID);
        bool Save(string filename, Stream file, DirectoryInfo dirDestination);
        bool Save(string filename, Stream file, Guid projectID, bool isLogo);
        bool Save(Guid projectID, IEnumerable<KeyValuePair<string, Stream>> files);
        bool Save(Guid projectID, IEnumerable<KeyValuePair<string, Stream>> files, out Dictionary<string, Stream> filesSaved, bool isLogo = false);
        bool Save(string filename, Stream file, Guid projectID, out Dictionary<string, Stream> filesSaved);

        IEnumerable<string> Get(Guid projectID);
        IEnumerable<string> Get(Guid projectID, bool withLogo);

        bool Delete(Guid projectID);
        bool Delete(Guid projectID, string fileName);

        void FormatIfNecessary(Guid projectID, string fileName, bool isLogo);
        void FormatIfNecessary(DirectoryInfo dirOriginal, DirectoryInfo dirTumbs, string fileName, bool isLogo = false);
        bool HasFiles(Guid projectId);
    }
}
