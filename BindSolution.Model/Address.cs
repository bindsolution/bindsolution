using BindSolution.Framework.Domain;

namespace BindSolution.Data
{
    using System;

    public class Address : IAuditable
    {
        public Guid ID { get; private set; }
        public string Display { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }
        public string Neighborhood { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Description { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        #region Implementation of IAuditable

        public DateTime CreatedAt { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public string UpdatedBy { get; set; }

        #endregion

        public Address()
        {
            ID = Guid.NewGuid();
        }
    }
}