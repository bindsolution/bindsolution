using System.IO;
using System.Web;
using AutoMapper;
using BindSolution.Infra;
using BindSolution.ViewModel;

namespace BindSolution.Mappers
{
    public class ProjectLogoResolver : ValueResolver<ProjectViewModel, string>
    {
        #region Overrides of ValueResolver<ProjectViewModel,string>

        protected override string ResolveCore(ProjectViewModel source)
        {
            return source.Logo != null
                       ? source.Logo.FileName
                       : Path.GetFileName(Directory.GetFiles(HttpContext.Current.Server.MapPath(string.Format(Constants.ProjectImagePathFormat, source.ID)), "*.*", SearchOption.TopDirectoryOnly)[0]);
        }

        #endregion
    }
}