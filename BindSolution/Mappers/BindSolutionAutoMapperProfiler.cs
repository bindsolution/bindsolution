#region copyright
// -----------------------------------------------------------------------
//  <copyright file="BindSolutionAutoMapperProfiler.cs" company="BindSolution">
//      All rights reserved BindSolution
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using BindSolution.Data;
using BindSolution.Framework.Web;
using BindSolution.ViewModel;

namespace BindSolution.Mappers
{
    public class BindSolutionAutoMapperProfiler : AutoMapperProfile
    {
        protected override void Configure()
        {
            //Map from View Model
            CreateMap<AddressViewModel, Address>()
                .ForMember(p => p.CreatedAt, opt => opt.Ignore())
                .ForMember(p => p.CreatedBy, opt => opt.Ignore())
                .ForMember(p => p.UpdatedAt, opt => opt.Ignore())
                .ForMember(p => p.UpdatedBy, opt => opt.Ignore())
                .ForMember(p => p.ID, opt => opt.Ignore());

            CreateMap<ProjectViewModel, Project>()
                .ForMember(p => p.CreatedAt, opt => opt.Ignore())
                .ForMember(p => p.CreatedBy, opt => opt.Ignore())
                .ForMember(p => p.UpdatedAt, opt => opt.Ignore())
                .ForMember(p => p.UpdatedBy, opt => opt.Ignore())
                .ForMember(p => p.Addresses, opt => opt.Ignore())
                .ForMember(p => p.Published, opt => opt.UseValue(false))
                .ForMember(p => p.Logo, opt => opt.ResolveUsing<ProjectLogoResolver>())
                .ForMember(p => p.Images, opt => opt.ResolveUsing<HttpPostedFileEnumerableStringCollectionResolver>());

            //Map from Domain model
            CreateMap<Project, ProjectViewModel>()
                .ForMember(p => p.Logo, opt => opt.Ignore())
                .ForMember(p => p.Images, opt => opt.Ignore());

            CreateMap<Address, AddressViewModel>();

            base.Configure();
        }
    }
}