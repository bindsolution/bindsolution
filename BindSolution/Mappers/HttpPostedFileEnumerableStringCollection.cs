using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using BindSolution.Data;
using BindSolution.ViewModel;

namespace BindSolution.Mappers
{
    public class HttpPostedFileEnumerableStringCollectionResolver : ValueResolver<ProjectViewModel, ICollection<ProjectImage>>
    {
        private readonly IProjectImageWrap _projectImageWrap;

        public HttpPostedFileEnumerableStringCollectionResolver(IProjectImageWrap projectImageWrap)
        {
            _projectImageWrap = projectImageWrap;
        }

        #region Overrides of ValueResolver<IEnumerable<HttpPostedFileBase>,ICollection<string>>

        protected override ICollection<ProjectImage> ResolveCore(ProjectViewModel source)
        {
            if (source.ID.HasValue)
                return _projectImageWrap.Get(source.ID.Value, false).Select(img => new ProjectImage { Name = Path.GetFileName(img) }).ToList();
            return null;
            /*
            return source.Images != null
                ? source.Images.Select(fileBase => new ProjectImage { Name = fileBase.FileName }).ToList()
                : null;
             */
        }

        #endregion
    }
}