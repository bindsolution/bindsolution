﻿// //-----------------------------------------------------------------------
// // <copyright file="ContactMailer.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Mailers
{
    #region usings

    using System.Net.Mail;
    using Infra;
    using Mvc.Mailer;
    using Service;

    #endregion

    /// <summary>
    /// Class for email contacts
    /// </summary>
    public sealed class ContactMailer : MailerBase, IEmailSend
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContactMailer"/> class.
        /// </summary>
        public ContactMailer()
        {
            MasterName = "_Layout";
        }

        #region Implementation of IEmailSend

        /// <summary>
        /// Sends the contact email.
        /// </summary>
        /// <param name="name">The contact name.</param>
        /// <param name="email">The email contact.</param>
        /// <param name="message">The message.</param>
        public void SendContactEmail(string name, string email, string message)
        {
            var mailMessage = new MailMessage
                                  {
                                      Subject = "[Bind Website] - " + name
                                  };

            ViewBag.Name = name;
            ViewBag.Email = email;
            ViewBag.Message = message;

            mailMessage.To.Add(Constants.EmailAddressSent);
            mailMessage.From = new MailAddress(email, name);

            PopulateBody(mailMessage, "ContactMailer");

            mailMessage.SendAsync();
        }

        #endregion
    }
}