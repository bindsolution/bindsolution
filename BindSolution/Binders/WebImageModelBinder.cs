#region copyright
// -----------------------------------------------------------------------
//  <copyright file="WebImageModelBinder.cs" company="BindSolution">
//      All rights reserved BindSolution
//  </copyright>
// -----------------------------------------------------------------------
#endregion

namespace BindSolution.Binders
{
    #region usings

    using System;
    using System.IO;
    using System.Linq;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using BindSolution.Infra;
    using BindSolution.Service;
    using Ninject;

    #endregion

    public class WebImageModelBinder : IModelBinder
    {
        [Inject]
        public IProjectService ProjectService { get; set; }

        #region Implementation of IModelBinder

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext == null)
                throw new ArgumentNullException("controllerContext");
            if (bindingContext == null)
                throw new ArgumentNullException("bindingContext");

            var projectID = controllerContext.HttpContext.Request["projectID"];
            var fileName = controllerContext.HttpContext.Request["fileName"];
            var imageID = controllerContext.HttpContext.Request["imageID"];

            if (string.IsNullOrWhiteSpace(fileName) && string.IsNullOrWhiteSpace(imageID))
            {
                bindingContext.ModelState.AddModelError("", "Imagem n�o fornecida.");
                return null;
            }



            if (string.IsNullOrWhiteSpace(projectID))
            {
                var projectUrl = controllerContext.RouteData.Values["url"];

                if (string.IsNullOrWhiteSpace(fileName))
                {
                    var data = (from p in ProjectService.Repository.Query()
                                where p.Keyword == projectUrl.ToString()
                                let image = p.Images.Where(i => i.ID == new Guid(imageID)).SingleOrDefault()
                                where image != null
                                select new
                                           {
                                               projectID = p.ID
                                               ,
                                               fileName = image.Name
                                           }).SingleOrDefault();




                    if (data != null)
                    {
                        projectID = data.projectID.ToString();
                        fileName = data.fileName;
                    }
                    else
                    {
                        bindingContext.ModelState.AddModelError("", "Imagem n�o encontrada.");
                        return null;
                    }
                }
                else
                {
                    var projectIDDb = ProjectService.GetID(projectUrl.ToString());
                    if (projectIDDb.HasValue)
                        projectID = projectIDDb.Value.ToString();
                    else
                    {
                        bindingContext.ModelState.AddModelError("", "Projeto n�o encontrado.");
                        return null;
                    }
                }
            }


            var filePath = string.Format(Constants.ProjectImageTumbsPathFormat, projectID);
            filePath = controllerContext.HttpContext.Server.MapPath(filePath);
            if (fileName != null) filePath = Path.Combine(filePath, fileName);

            if (!File.Exists(filePath))
            {
                bindingContext.ModelState.AddModelError("", "Arquivo de imagem n�o encontrado.");
                return null;
            }

            return new WebImage(filePath);

        }

        #endregion
    }
}