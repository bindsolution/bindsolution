﻿// //-----------------------------------------------------------------------
// // <copyright file="Constants.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Infra
{
    /// <summary>
    /// All consts for Web Application
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Project full image path format
        /// </summary>
        /// <example>
        /// string.format(Constants.ProjectImagePathFormat, project.ID) + "image-fileName"
        /// </example>
        public const string ProjectImagePathFormat = "~/Images/ProjectImages/{0}/";

        /// <summary>
        /// Project tumbs image path format
        /// </summary>
        /// <example>
        /// string.format(Constants.ProjectImagePathFormat, project.ID) + "image-fileName"
        /// </example>
        public const string ProjectImageTumbsPathFormat = "~/Images/ProjectImages/{0}/Tumbs/";

        /// <summary>
        /// Twitter key API
        /// </summary>
        public const string TwitterApp = "EunN3LrclOH2OPqAfhmaQ";

        /// <summary>
        /// Account used for send email
        /// </summary>
        public const string EmailAddressSent = "contato@bindsolution.com";
    }
}