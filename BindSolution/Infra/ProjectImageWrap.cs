// //-----------------------------------------------------------------------
// // <copyright file="ProjectImageWrap.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Infra
{
    #region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Web;
    using System.Web.Helpers;
    using Data;
    using Framework.Web.Extensions;
    using Ionic.Zip;

    #endregion

    public class ProjectImageWrap : IProjectImageWrap
    {
        #region Implementation of IProjectImageWrap<WebImage>

        public bool Save(Guid projectID)
        {
            return Save(projectID, HttpContext.Current.Request.GetFiles());
        }

        public bool Save(Guid projectID, IEnumerable<KeyValuePair<string, Stream>> files)
        {
            Dictionary<string, Stream> filesSaved;
            return Save(projectID, files, out filesSaved);
        }

        public bool Save(Guid projectID, IEnumerable<KeyValuePair<string, Stream>> files,
                         out Dictionary<string, Stream> filesSaved, bool isLogo = false)
        {
            if (files == null) throw new ArgumentNullException("files");

            var dirOriginal =
                new DirectoryInfo(
                    HttpContext.Current.Server.MapPath(string.Format(Constants.ProjectImagePathFormat, projectID)));
            var dirTumbs =
                new DirectoryInfo(
                    HttpContext.Current.Server.MapPath(string.Format(Constants.ProjectImageTumbsPathFormat, projectID)));
            filesSaved = new Dictionary<string, Stream>();

            //Original directory
            if (!dirOriginal.Exists)
                dirOriginal.Create();

            //Tumbs directory
            if (!dirTumbs.Exists)
                dirTumbs.Create();

            //Request files
            foreach (var rFile in files)
            {
                if (rFile.Value == null)
                    throw new ApplicationException("Arquivo inv�lido");

                if (ZipFile.IsZipFile(rFile.Value, true))
                {
                    if (rFile.Value.Position != 0)
                        rFile.Value.Seek(0, SeekOrigin.Begin);
                    using (var zip = ZipFile.Read(rFile.Value))
                    {
                        foreach (var zipFile in zip)
                        {
                            zipFile.Extract(dirOriginal.FullName, ExtractExistingFileAction.Throw);
                            FormatIfNecessary(dirOriginal, dirTumbs, zipFile.FileName, isLogo);
                            filesSaved.Add(zipFile.FileName, zipFile.InputStream);
                        }
                    }
                }
                else
                {
                    Save(rFile.Key, rFile.Value, dirOriginal);
                    FormatIfNecessary(dirOriginal, dirTumbs, rFile.Key, isLogo);
                    filesSaved.Add(rFile.Key, rFile.Value);
                }
            }

            return true;
        }

        public bool Save(string filename, Stream file, Guid projectID, out Dictionary<string, Stream> filesSaved)
        {
            var dic = new Dictionary<string, Stream> { { filename, file } };
            return Save(projectID, dic, out filesSaved);
        }

        public bool Save(string filename, Stream file, Guid projectID, bool isLogo = false)
        {
            var dic = new Dictionary<string, Stream> { { filename, file } };
            Dictionary<string, Stream> filesSaved;
            return Save(projectID, dic, out filesSaved, isLogo);
        }

        public bool Save(string filename, Stream file, DirectoryInfo dirDestination)
        {
            if (file == null)
                throw new ArgumentNullException("file");


            if (!dirDestination.Exists)
                dirDestination.Create();

            var buffer = new byte[file.Length];
            file.Read(buffer, 0, buffer.Length);
            File.WriteAllBytes(Path.Combine(dirDestination.FullName, filename), buffer);
            return true;
        }

        public IEnumerable<string> Get(Guid projectID)
        {
            return Get(projectID, true);
        }

        public IEnumerable<string> Get(Guid projectID, bool withLogo)
        {
            var server = HttpContext.Current.Server;

            var dirOriginal =
                new DirectoryInfo(server.MapPath(string.Format(Constants.ProjectImagePathFormat, projectID)));
            var dirTumbs =
                new DirectoryInfo(server.MapPath(string.Format(Constants.ProjectImageTumbsPathFormat, projectID)));

            foreach (
                var fileInfo in
                    dirOriginal.GetFiles("*.*", SearchOption.TopDirectoryOnly).Where(
                        fileInfo => withLogo || !fileInfo.Name.ToLower().StartsWith("logo")))
            {
                FormatIfNecessary(dirOriginal, dirTumbs, fileInfo.Name);
                yield return fileInfo.Name;
            }
        }

        public void FormatIfNecessary(Guid projectID, string fileName, bool isLogo = false)
        {
            var server = HttpContext.Current.Server;
            var dirTumbs =
                new DirectoryInfo(server.MapPath(string.Format(Constants.ProjectImageTumbsPathFormat, projectID)));
            var dirOriginal =
                new DirectoryInfo(server.MapPath(string.Format(Constants.ProjectImagePathFormat, projectID)));

            FormatIfNecessary(dirOriginal, dirTumbs, fileName);
        }

        public void FormatIfNecessary(DirectoryInfo dirOriginal, DirectoryInfo dirTumbs, string fileName, bool isLogo = false)
        {
            var fullPath = Path.Combine(dirOriginal.FullName, fileName);

            if (dirTumbs.EnumerateFiles(fileName).Any()) return;

            var tumb = new WebImage(fullPath);
            if (!isLogo)
            {
                if (tumb.Width > 214 || tumb.Height > 154)
                    tumb.Resize(214, 154);

                new WebImage(fullPath)
                    .AddTextWatermark("bindsolution.com", "#414141", opacity: 80)
                    .Save();
            }
            else
            {
                if (tumb.Width > 214 || tumb.Height > 60)
                    tumb.Resize(214, 60);
            }

            tumb.Save(Path.Combine(dirTumbs.FullName, fileName));


        }

        public bool HasFiles(Guid projectId)
        {
            var dirOriginal =
                new DirectoryInfo(
                    HttpContext.Current.Server.MapPath(string.Format(Constants.ProjectImagePathFormat, projectId)));

            return dirOriginal.Exists && dirOriginal.GetFiles().Length > 0;
        }

        public bool Delete(Guid projectID)
        {
            var dirOriginal =
                new DirectoryInfo(
                    HttpContext.Current.Server.MapPath(string.Format(Constants.ProjectImagePathFormat, projectID)));

            //Project directory
            if (dirOriginal.Exists)
            {
                try
                {
                    dirOriginal.Delete(true);
                }
                catch (IOException)
                {
                    Thread.Sleep(200);
                    dirOriginal.Delete(true);
                }
            }

            return true;
        }

        public bool Delete(Guid projectID, string fileName)
        {
            var dirOriginal =
                new DirectoryInfo(
                    HttpContext.Current.Server.MapPath(string.Format(Constants.ProjectImagePathFormat, projectID)));
            var pathImage = Path.Combine(dirOriginal.FullName, fileName);
            if (File.Exists(pathImage))
                File.Delete(pathImage);

            var dirTumbs =
                new DirectoryInfo(
                    HttpContext.Current.Server.MapPath(string.Format(Constants.ProjectImageTumbsPathFormat, projectID)));
            pathImage = Path.Combine(dirTumbs.FullName, fileName);
            if (File.Exists(pathImage))
                File.Delete(pathImage);

            return true;
        }

        #endregion
    }
}