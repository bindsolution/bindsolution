using BindSolution.ViewModel;
using FluentValidation;

namespace BindSolution.Validators
{
    public class AddressValidator : AbstractValidator<AddressViewModel>
    {
        public AddressValidator()
        {
            RuleFor(p => p.Display)
                .NotEmpty().WithMessage("campo obrigat�rio")
                .Length(5, 60).WithMessage("deve conter entre {0} e {1} caract�res", 5, 60);

            RuleFor(p => p.Street)
                .NotEmpty().WithMessage("campo obrigat�rio")
                .Length(5, 120).WithMessage("deve conter entre {0} e {1} caract�res", 5, 120);

            RuleFor(p => p.Number)
                .NotEmpty().WithMessage("campo obrigat�rio")
                .Length(1, 35).WithMessage("deve conter entre {0} e {1} caract�res", 1, 35);

            RuleFor(p => p.Complement)
                .Length(0, 35).WithMessage("deve conter entre {0} e {1} caract�res", 0, 35);

            RuleFor(p => p.Latitude)
                .NotEmpty().WithMessage("{PropertyName} � obrigat�rio");
            RuleFor(p => p.Longitude)
                .NotEmpty().WithMessage("{PropertyName} � obrigat�rio");

            RuleFor(p => p.City)
                .NotEmpty().WithMessage("campo obrigat�rio")
                .Length(2, 120).WithMessage("deve conter entre {0} e {1} caract�res", 2, 120);

            RuleFor(p => p.State)
                .NotEmpty().WithMessage("campo obrigat�rio")
                .Length(2, 80).WithMessage("deve conter entre {0} e {1} caract�res", 2, 80);
        }
    }
}