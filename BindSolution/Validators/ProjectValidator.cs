﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="ProjectCreateValidator.cs" company="BindSolution">
//      All rights reserved BindSolution
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using System.Linq;
using System.Web;
using BindSolution.Data;
using BindSolution.Service;
using BindSolution.ViewModel;
using FluentValidation;
using Ninject;

namespace BindSolution.Validators
{
    public class ProjectValidator : AbstractValidator<ProjectViewModel>
    {
        private readonly IProjectService _projectService;
        private readonly IProjectImageWrap _projectImageWrap;

        [Inject]
        public ProjectValidator(IProjectService projectService, IProjectImageWrap projectImageWrap)
        {
            _projectService = projectService;
            _projectImageWrap = projectImageWrap;

            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("campo obrigatório")
                .Length(5, 60).WithMessage("deve conter entre {0} e {1} caractéres", 5, 60);
            RuleFor(p => p.Name)
                .Must(n => !_projectService.Exist(n)).WithMessage("{PropertyName} já está sendo utilizado")
                .When(p => !p.ID.HasValue || !_projectService.Exist(p.ID.Value));

            RuleFor(p => p.WebSite)
                .NotEmpty().WithMessage("campo obrigatório")
                .Length(10, 180).WithMessage("deve conter entre {0} e {1} caractéres", 10, 180)
                .Matches(@"^http\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$").WithMessage("{PropertyName} inválido");

            RuleFor(p => p.Keyword)
                .NotEmpty().WithMessage("campo obrigatório")
                .Length(5, 35).WithMessage("deve conter entre {0} e {1} caractéres", 5, 35);
            RuleFor(p => p.Keyword)
                .Must(n => !_projectService.KeywordExist(n)).WithMessage("{PropertyName} já está sendo utilizado")
                .When(p => !p.ID.HasValue || !_projectService.Exist(p.ID.Value));
            //.Matches(@"/^[a-zA-Z0-9_\-\.\+]$/").WithMessage("formato inválido");

            RuleFor(p => p.Rate)
                .NotEmpty().WithMessage("campo obrigatório")
                .InclusiveBetween((short)0, (short)10).WithMessage("deve estar entre {0} e {1}", 0, 10);

            RuleFor(p => p.Description)
                .Length(5, 255).WithMessage("deve conter entre {0} e {1} caractéres", 5, 255)
                .NotEmpty().WithMessage("campo obrigatório");

            RuleFor(p => p.Details)
                            .NotEmpty().WithMessage("campo obrigatório");

            RuleFor(p => p.Logo)
                .NotEmpty().WithMessage("campo obrigatório")
                .SetValidator(new HttpPostedFileValidator())
                .When(p => p.ID != null && ((!_projectService.Exist(p.ID.Value)) && !_projectImageWrap.HasFiles(p.ID.Value)));

        }
    }

    public class HttpPostedFileValidator : AbstractValidator<HttpPostedFileBase>
    {
        public HttpPostedFileValidator(params string[] invalidNames)
        {
            RuleFor(p => p.FileName)
                .NotEmpty().WithMessage("nome do arquivo não foi localizado")
                .Must(p => !invalidNames.Any(p.StartsWith)).WithMessage("nome do arquivo inválido")
                .When(p => invalidNames != null && invalidNames.Count() > 0);

            RuleFor(p => p.ContentLength)
                .GreaterThan(1).WithMessage("arquivo inválido");

            RuleFor(p => p.InputStream)
                            .NotEmpty().WithMessage("nome do arquivo não foi localizado");
        }
    }
}