﻿// //-----------------------------------------------------------------------
// // <copyright file="LogOnViewModelExtensions.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Extensions
{
    #region usings

    using System.Linq;
    using Framework;
    using Framework.Domain;
    using Framework.Service;
    using Framework.Web.ViewModel;

    #endregion

    public static class LogOnViewModelExtensions
    {
        public static User RepositoryUser(this LogOnViewModel model, IUserService service)
        {
            var pass = model.Password.Sha1Hash().ToLower();
            return (from u in service.Repository.Query()
                    where u.Email == model.Email && u.Password == pass
                    select u).SingleOrDefault();
        }
    }
}