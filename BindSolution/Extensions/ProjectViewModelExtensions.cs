﻿// //-----------------------------------------------------------------------
// // <copyright file="ProjectViewModelExtensions.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Extensions
{
    #region usings

    using System;
    using System.IO;
    using System.Linq;
    using System.Web;
    using Data;
    using Infra;
    using Service;
    using ViewModel;

    #endregion

    /// <summary>
    /// Extensions for ProjectViewModel
    /// </summary>
    public static class ProjectViewModelExtensions
    {
        public static void SaveLogo(this ProjectViewModel model, IProjectImageWrap imageWrap)
        {
            if (model.ID == null) throw new ArgumentNullException("model");

            if (model.Logo != null)
                imageWrap.SaveLogo(model.ID.Value, model.Logo.FileName, model.Logo.InputStream);
        }

        public static void SaveLogo(this IProjectImageWrap imageWrap, Guid projectID, string fileName, Stream stream)
        {
            imageWrap.Save(fileName, stream, projectID, true);
        }

        public static void ReplaceLogo(this IProjectImageWrap imageWrap, Guid projectID, IProjectService projectService, string fileName, Stream stream)
        {
            var oldLogo = projectService.GetLogo(projectID);
            if (!string.IsNullOrWhiteSpace(oldLogo))
            {
                var path = HttpContext.Current.Server.MapPath(
                    Path.Combine(string.Format(Constants.ProjectImagePathFormat, projectID), oldLogo));

                var pathTumbs = HttpContext.Current.Server.MapPath(
                    Path.Combine(string.Format(Constants.ProjectImageTumbsPathFormat, projectID), oldLogo));

                if (File.Exists(path))
                    File.Delete(path);

                if (File.Exists(pathTumbs))
                    File.Delete(pathTumbs);
            }
            imageWrap.SaveLogo(projectID, fileName, stream);
        }

        public static void ReplaceLogo(this IProjectImageWrap imageWrap, Project project, IProjectService projectService, HttpPostedFileBase newLogo)
        {
            imageWrap.ReplaceLogo(project.ID, projectService, newLogo.FileName, newLogo.InputStream);
            project.Logo = newLogo.FileName;
        }

        public static void SaveImages(this ProjectViewModel model, IProjectImageWrap imageWrap)
        {
            if (model.ID == null) throw new ArgumentNullException("model");

            if (model.Images != null)
                imageWrap.Save(model.ID.Value,
                               model.Images.ToDictionary(item => item.FileName, item => item.InputStream));
        }
    }
}