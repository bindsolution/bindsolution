// //-----------------------------------------------------------------------
// // <copyright file="UrlHelperExtensions.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Extensions
{
    #region usings

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// Extensions for UrlHelper
    /// </summary>
    public static class UrlHelperExtensions
    {
        /// <summary>
        /// Determines whether [is return URL] [the specified URL].
        /// </summary>
        /// <param name="url">The URL Helper.</param>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns>
        ///   <c>true</c> if [is return URL] [the specified URL]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsReturnUrl(this UrlHelper url, string returnUrl)
        {
            return url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") &&
                   !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\");
        }
    }
}