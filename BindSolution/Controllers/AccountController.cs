﻿// //-----------------------------------------------------------------------
// // <copyright file="AccountController.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author></author>
// //-----------------------------------------------------------------------

using AutoMapper;

namespace BindSolution.Controllers
{
    #region usings

    using System.Web.Mvc;
    using System.Web.Security;
    using Extensions;
    using Framework.Service;
    using Framework.Web;
    using Framework.Web.Extensions;
    using Framework.Web.ViewModel;

    #endregion

    /// <summary>
    /// Users Account controller
    /// </summary>
    public class AccountController : BindController
    {
        /// <summary>
        /// User service
        /// </summary>
        private readonly IUserService _userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        /// <param name="userService">The user service.</param>
        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Logs the on.
        /// </summary>
        /// <returns>ActionResult view</returns>
        public ActionResult LogOn()
        {
            return View();
        }

        /// <summary>
        /// Logs the on.
        /// </summary>
        /// <param name="model">The LogOnViewModel.</param>
        /// <param name="returnUrl">The return URL after auth.</param>
        /// <returns>ActionResult view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOn(LogOnViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var userDB = model.RepositoryUser(_userService);
                var user = Mapper.Map(model, userDB);
                Response.LogIn(user, model.RememberMe);

                return Url.IsReturnUrl(returnUrl)
                           ? AjaxRedirect(returnUrl)
                           : AjaxRedirect("Index", "Home");
            }

            return AjaxResult();
        }

        /// <summary>
        /// Logs the out.
        /// </summary>
        /// <returns>ActionResult view</returns>
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return AjaxRedirect("Index", "Home");
        }
    }
}