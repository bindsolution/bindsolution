﻿// //-----------------------------------------------------------------------
// // <copyright file="ErrorController.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author></author>
// //-----------------------------------------------------------------------

namespace BindSolution.Controllers
{
    #region usings

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// Error Controller
    /// </summary>
    public class ErrorController : Controller
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>ActionResult view</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Error404s this instance.
        /// </summary>
        /// <returns>ActionResult view</returns>
        public ActionResult Error404()
        {
            return View();
        }
    }
}