﻿// //-----------------------------------------------------------------------
// // <copyright file="HomeController.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author></author>
// //-----------------------------------------------------------------------


namespace BindSolution.Controllers
{
    #region usings

    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ServiceModel.Syndication;
    using Data.Extensions;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Xml;
    using System.Xml.Linq;
#if DEBUG
    using Framework;
#else
    using System.IO;
    using System.Xml;
    using System.Net;
    using System.Xml.Linq;
    using Framework;
    using System.Linq;
#endif
    using System.Web.Mvc;
    using Data.Infra;
    using Framework.Domain;
    using Framework.Service;
    using Framework.Web;
    using Framework.Web.Filters;
    using Framework.Web.ViewModel;
    using Service;
    using ViewModel;

    #endregion

    public class HomeController : BindAsyncController
    {
        private readonly IEmailSend _emailSend;
        private readonly IUserService _userService;
        private readonly IProjectService _projectService;

        #region Constructor

        public HomeController(IUserService userService, IEmailSend emailSend, IProjectService projectService)
        {
            _userService = userService;
            _emailSend = emailSend;
            _projectService = projectService;
        }

        #endregion

        #region Actions

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                _emailSend.SendContactEmail(model.Name, model.Email, model.Menssage);
                return AjaxResult(model, "Mensagem enviada com sucesso!", "Em breve entraremos em contato.");
            }
            return AjaxResult(model);
        }

        public ActionResult Plus()
        {
            return View();
        }

        public ActionResult PlusBusiness()
        {
            return View();
        }

        public ActionResult PlusTech()
        {
            return View();
        }

        public ActionResult Why()
        {
            return View();
        }

        #endregion

        #region Partials

        [ChildActionOnly]
        [CustomOutputCache(CacheProfile = "InternalResource")]
        public PartialViewResult ProjectList()
        {
            var projects = _projectService.Repository.Query()
                .OnlyPublishedProjects()
                .WithDefaultOrdered()
                .Take(3);

            return PartialView("_Projects", projects.ToList());
        }

        [ChildActionOnly]
        [OnlyIfNonJava]
        [CustomOutputCache(CacheProfile = "ExternalResource")]
        public void BlogRssAsync(int page = 0)
        {
            AsyncManager.OutstandingOperations.Increment(1);

            var bg = new BackgroundWorker();
            bg.DoWork += (o, e) =>
                             {
                                 int pg;
                                 if (e.Argument != null && int.TryParse(e.Argument.ToString(), out pg))
                                     AsyncManager.Parameters["items"] = e.Result = GetFeeds(pg);
                                 else
                                     AsyncManager.Parameters["items"] = e.Result = GetFeeds();
                             };
            bg.RunWorkerCompleted += (o, e) => AsyncManager.OutstandingOperations.Decrement();
            bg.RunWorkerAsync(page);
        }

        [ChildActionOnly]
        [CustomOutputCache(CacheProfile = "ExternalResource")]
        public PartialViewResult BlogRssCompleted(IEnumerable<SyndicationItem> items)
        {
            return PartialView("_BlogRss", items);
        }

        [ChildActionOnly]
        [OnlyIfNonJava]
        [CustomOutputCache(CacheProfile = "ExternalResource")]
        public void TwitterAsync()
        {
            AsyncManager.OutstandingOperations.Increment(1);

            var bg = new BackgroundWorker();
            bg.DoWork += (o, e) => AsyncManager.Parameters["items"] = e.Result = GetTwitters();
            bg.RunWorkerCompleted += (o, e) => AsyncManager.OutstandingOperations.Decrement();
            bg.RunWorkerAsync();
        }

        [ChildActionOnly]
        [CustomOutputCache(CacheProfile = "ExternalResource")]
        public PartialViewResult TwitterCompleted(List<TwitterDataViewModel> items)
        {
            return PartialView("_Twitter", items);
        }

        [NonAction]
        public IEnumerable<SyndicationItem> GetFeeds(int page = 0)
        {
            try
            {
                using (var reader = XmlReader.Create("http://blog.bindsolution.com/rss"))
                {
                    var rssData = SyndicationFeed.Load(reader);
                    if (rssData != null)
                    {
                        return (from item in rssData.Items
                                orderby item.PublishDate descending
                                select item).Take(3).Skip(3 * page).ToList();
                    }
                    return null;
                }
            }
            catch (WebException wex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(wex);
                return null;
            }
        }

        #endregion

        [NonAction]
        public IEnumerable<TwitterDataViewModel> GetTwitters()
        {
            var request = WebRequest.Create("http://search.twitter.com/search.atom?q=bindsolution&rpp=5") as HttpWebRequest;

            if (request != null)
            {
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        var document = XDocument.Parse(reader.ReadToEnd());
                        XNamespace xmlns = "http://www.w3.org/2005/Atom";

                        return (from entry in document.Descendants(xmlns + "entry")
                                let content = entry.Element(xmlns + "content")
                                where content != null
                                let update = entry.Element(xmlns + "updated")
                                where update != null
                                let author = entry.Element(xmlns + "author")
                                where author != null
                                let name = author.Element(xmlns + "name")
                                where name != null
                                let uri = author.Element(xmlns + "uri")
                                where uri != null
                                let id = entry.Element(xmlns + "id")
                                where id != null
                                select new TwitterDataViewModel
                                           {
                                               Content = content.Value,
                                               Updated = DateTime.Parse(update.Value).PrettyFormat(),
                                               AuthorName = name.Value,
                                               AuthorUri = uri.Value,
                                               TwitterID = id.Value,
                                               Link = (from o in entry.Descendants(xmlns + "link")
                                                       let xAttribute = o.Attribute("rel")
                                                       where xAttribute != null && xAttribute.Value == "image"
                                                       let attribute = o.Attribute("href")
                                                       where attribute != null
                                                       select new { Val = attribute.Value }).First().Val
                                           }).ToList();
                    }
                }
            }
            return null;
        }

        [Authorize(Roles = RoleConstants.Administrators)]
        public ActionResult ThrowError()
        {
            throw new ApplicationException("Forced error!!");
        }

        [Authorize(Roles = RoleConstants.Administrators)]
        public JsonResult AddUser(string user, string email, string pass)
        {
            try
            {
                var userObj = new User
                                  {
                                      Name = user
                                      ,
                                      Password = pass.Sha1Hash()
                                      ,
                                      IsApproved = true
                                      ,
                                      IsBlock = false
                                      ,
                                      Email = email
                                  };
                _userService.Repository.Add(userObj);
                return Json(_userService.Repository.Save() > 0
                                ? new { success = true, message = "Usuário cadastrado com sucesso!" }
                                : new { success = false, message = "Não foi possível realizar o cadastro do usuário." },
                            JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.GetInnerException().Message });
            }
        }

    }
}