﻿// //-----------------------------------------------------------------------
// // <copyright file="ProjectController.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author></author>
// //-----------------------------------------------------------------------



namespace BindSolution.Controllers
{
    #region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using AutoMapper;
    using Binders;
    using Data;
    using Data.Extensions;
    using Data.Infra;
    using Extensions;
    using Filters;
    using Framework;
    using Framework.Web;
    using Framework.Web.Extensions;
    using Infra;
    using Microsoft.Security.Application;
    using Service;
    using ViewModel;

    #endregion

    public class ProjectController : BindController
    {
        #region Properties

        public IProjectService ProjectService { get; private set; }
        private readonly IProjectImageWrap _projectImageWrap;
        private readonly IAddressRepository _addressRepository;
        private readonly IProjectImageRepository _projectImageRepository;

        #endregion

        #region Constructor

        public ProjectController(IProjectService projectService, IProjectImageWrap projectImageWrap, IAddressRepository addressRepository, IProjectImageRepository projectImageRepository)
        {
            ProjectService = projectService;
            _projectImageWrap = projectImageWrap;
            _addressRepository = addressRepository;
            _projectImageRepository = projectImageRepository;
        }

        #endregion

        public ActionResult Index()
        {
            var list = ProjectService.Repository.Query().ToList();
            return View(list);
        }

        [ProjectHeader]
        public ActionResult About(Project project)
        {
            return View(project);
        }

        [ProjectHeader]
        public ActionResult Photos(Project project)
        {
            ViewBag.PathImageTumbsFormat = Path.Combine(string.Format(Constants.ProjectImageTumbsPathFormat, project.ID), "{0}");
            var pathFullImage = string.Format(Constants.ProjectImagePathFormat, project.ID);
            ViewBag.PathImageFormat = pathFullImage.Remove(pathFullImage.LastIndexOf('/'), 1);
            var images = project.Images.Select(p => new WebImage(Server.MapPath(Path.Combine(string.Format(Constants.ProjectImageTumbsPathFormat, project.ID), p.Name))));
            ViewData.Add("Images", images.ToList());
            return View(project);

        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Photos(Guid id)
        {
            if (ModelState.IsValid)
            {
                foreach (var file in Request.GetFiles())
                {
                    Dictionary<string, Stream> filesSaved;
                    _projectImageWrap.Save(file.Key, file.Value, id, out filesSaved);
                    var project = ProjectService.Repository.Get(id);
                    foreach (var fileSave in filesSaved)
                        ProjectService.AddImage(fileSave.Key, project);
                }

                if (ProjectService.Repository.Save() > 0)
                    return AjaxRedirect("Photos");
            }
            return View();
        }

        [ProjectHeader]
        public ActionResult Addresses(Project project)
        {
            return Request.IsAjaxRequest()
                       ? (ActionResult)Json(project.Addresses.ToList(), JsonRequestBehavior.AllowGet)
                       : View(project);
        }

        [ProjectHeader]
        public ActionResult Admin(Project project)
        {
            var pathFullImage = string.Format(Constants.ProjectImagePathFormat, project.ID);
            ViewBag.PathImageFormat = pathFullImage.Remove(pathFullImage.LastIndexOf('/'), 1);
            return View(project);
        }

        #region Project

        [Authorize(Roles = RoleConstants.Administrators)]
        public ActionResult Create()
        {
            return View(new ProjectViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create(ProjectViewModel model, bool redirect = true)
        {
            if (ModelState.IsValid)
            {
                model.Details = Sanitizer.GetSafeHtmlFragment(model.Details);
                model.SaveLogo(_projectImageWrap);

                var project = Mapper.Map<Project>(model);
                ProjectService.Repository.Add(project);



                if (ProjectService.Repository.Save() > 0)
                {
                    if (!redirect && Request.IsAjaxRequest())
                    {
                        var relativeLogoPath = string.Format(Path.Combine(Constants.ProjectImageTumbsPathFormat, "{1}"), project.ID, project.Logo);
                        var webImage = new WebImage(Server.MapPath(relativeLogoPath));
                        return Json(new
                                        {
                                            project,
                                            success = true,
                                            img = new { relativeLogoPath = Url.Content(relativeLogoPath), webImage.Width, webImage.Height },
                                            url = Url.Action("About", new { url = project.Keyword }),
                                            message = "Projeto cadastrado com suscesso!"
                                        });
                    }
                    return AjaxRedirect("Index");
                }
            }

            return AjaxResult(model);
        }


        [Authorize]
        [ProjectHeader]
        public ActionResult Edit(Project project)
        {
            ViewBag.Project = project;
            return View(Mapper.Map<ProjectViewModel>(project));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)] //TODO: REFACTOR: Desabilita toda a validação de HTML (Não recomendado!!)
        [Authorize]
        [ProjectHeader]
        public ActionResult Edit(ProjectViewModel model, Project project)
        {
            if (ModelState.IsValid && TryUpdateModel(project))
            {
                if (model.Logo != null)
                    _projectImageWrap.ReplaceLogo(project, ProjectService, model.Logo);

                project.Details = Sanitizer.GetSafeHtmlFragment(project.Details);
                if (ProjectService.Repository.Save() > 0)
                    return AjaxRedirect("About");
            }

            ViewBag.Project = project;
            return AjaxResult(model);
        }

        [ProjectHeader]
        [Authorize]
        public ActionResult Delete(Project project)
        {
            return View(project);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ProjectHeader]
        [Authorize]
        public ActionResult Delete(Project project, Guid id)
        {
            if (ModelState.IsValid)
            {
                ProjectService.Repository.Remove(project);
                if (_projectImageWrap.Delete(id) && ProjectService.Repository.Save() > 0)
                    return AjaxRedirect("Index");
                ModelState.AddModelError("", "Não foi possível remover o projeto");
            }

            return AjaxResult(project);
        }


        [Authorize]
        [ProjectHeader]
        public ActionResult Publish(Project project)
        {
            return View(project);
        }

        [HttpPost]
        [ProjectHeader]
        [Authorize]
        public ActionResult Publish(Project project, Guid? id, bool redirect = true)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    project.Published = !project.Published;

                    if (ProjectService.Repository.Save() > 0)
                    {
                        return !redirect && Request.IsAjaxRequest()
                                   ? Json(
                                       new
                                           {
                                               published = project.Published,
                                               keyword = project.Keyword,
                                               success = true,
                                               message = "Projeto " + (project.Published ? "publicado" : "despublicado") +
                                           " com suscesso!"
                                           })
                                   : AjaxRedirect("Admin");
                    }
                }
                ModelState.AddModelError("", "Não foi possível publicar o projeto");
                return AjaxResult(project);
            }
            catch (Exception ex)
            {
                return Json(new { keyword = project.Keyword, success = false, message = ex.GetInnerException().Message });
            }
        }

        #endregion

        #region Project Address

        [Authorize]
        [ProjectHeader]
        public ActionResult CreateAddress(Project project)
        {
            ViewBag.Project = project;
            return View(new AddressViewModel());
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)] //TODO: REFACTOR: Desabilita toda a validação de HTML (Não recomendado!!)
        [ProjectHeader(LoadAddresses = true)]
        public ActionResult CreateAddress(Project project, AddressViewModel model, string url)
        {
            if (ModelState.IsValid)
            {
                var address = Mapper.Map<Address>(model);
                ViewBag.Project = project;

                address.Description = Sanitizer.GetSafeHtmlFragment(model.Description);
                project.Addresses.Add(address);
                if (ProjectService.Repository.Save() > 0)
                    return AjaxRedirect("Addresses");
                ModelState.AddModelError("", "Não foi possível salvar o enderço");
            }

            return AjaxResult(model);
        }

        [Authorize]
        [ProjectHeader]
        public ActionResult EditAddress(Project project, Guid id)
        {
            ViewBag.Project = project;

            var address = ProjectService.GetAddress(project, id);

            return View(Mapper.Map<AddressViewModel>(address));
        }

        [HttpPost]
        [Authorize]
        [ProjectHeader]
        [ValidateInput(false)] //TODO: REFACTOR: Desabilita toda a validação de HTML (Não recomendado!!)
        public ActionResult EditAddress(AddressViewModel model, Guid id)
        {
            var address = _addressRepository.Get(id);

            if (ModelState.IsValid && TryUpdateModel(address))
            {
                address.Description = Sanitizer.GetSafeHtmlFragment(model.Description);
                if (_addressRepository.Save() > 0)
                    return AjaxRedirect("Addresses");
            }

            return AjaxResult(model);
        }

        [Authorize]
        [ProjectHeader]
        public ActionResult DeleteAddress(Project project, Guid id)
        {
            ViewBag.Project = project;

            var adrress = ProjectService.GetAddress(project, id);

            return View(Mapper.Map<AddressViewModel>(adrress));
        }

        [HttpPost]
        [Authorize]
        [ProjectHeader]
        public ActionResult DeleteAddress(Project project, Guid id, AddressViewModel model)
        {
            var adrress = _addressRepository.Get(id);
            _addressRepository.Remove(adrress);
            if (_addressRepository.Save() > 0)
                return AjaxRedirect("Addresses");
            ModelState.AddModelError("", "Não foi possível remover o endereço");
            return AjaxResult(model);
        }

        #endregion

        #region Project Images

        [Authorize]
        public ActionResult DeleteImage([ModelBinder(typeof(WebImageModelBinder))] WebImage image)
        {
            var fileName = Path.GetFileName(image.FileName);
            var projectID = Request["projectID"];

            ViewBag.RelativePathImage = string.Format(Constants.ProjectImageTumbsPathFormat, projectID) + fileName;
            return View(image);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteImage(Guid projectID, [ModelBinder(typeof(WebImageModelBinder))] WebImage image, string returnUrl = null)
        {
            try
            {
                var project = ProjectService.Repository.Get(projectID);

                if (project == null)
                    return Json(new { success = false, message = "Projeto não encontrado." });

                var imgID = (from imgDb in project.Images
                             where imgDb.Name == Path.GetFileName(image.FileName)
                             select imgDb.ID).FirstOrDefault();

                if (imgID == Guid.Empty)
                    return Json(new { success = false, message = "Imagem não encontrada." });

                var imageDelete = _projectImageRepository.Get(imgID);
                _projectImageRepository.Remove(imageDelete);


                if (_projectImageRepository.Save() > 0 && _projectImageWrap.Delete(project.ID, imageDelete.Name))
                {
                    if (returnUrl != null && Url.IsReturnUrl(returnUrl))
                        return Redirect(returnUrl);
                    return RedirectToAction("Admin");
                }
                return View();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { success = false, message = "Ocorreu um erro interno. Nossa equipe já foi notificada!" });
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult LogoUpload(Guid projectID)
        {
            try
            {
                var project = ProjectService.Repository.Get(projectID);
                foreach (var file in Request.GetFiles())
                {
                    _projectImageWrap.ReplaceLogo(projectID, ProjectService, file.Key, file.Value);
                    if (project != null)
                    {
                        project.Logo = file.Key;
                        ProjectService.Repository.Save();
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { success = false, message = "nenhum arquivo encontrado." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.GetInnerException().Message },
                            JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult ImagesUpload(Guid projectID)
        {
            var files = Request.GetFiles();
            try
            {
                var retOk = _projectImageWrap.Save(projectID, files);
                object image = null;
                if (retOk)
                {
                    var project = ProjectService.Repository.Get(projectID);
                    foreach (var file in files)
                    {
                        var pathFullImage = string.Format(Constants.ProjectImagePathFormat, projectID);
                        pathFullImage = Url.Content(Path.Combine(pathFullImage, file.Key));
                        image = new
                        {
                            fileName = file.Key
                            ,
                            projectID
                            ,
                            fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file.Key)
                            ,
                            pathFullImage
                        };
                        ProjectService.AddImage(file.Key, project);
                    }
                    retOk = ProjectService.Repository.Save() > 0;
                }

                return retOk
                           ? Json(new { success = true, image }, JsonRequestBehavior.AllowGet)
                           : Json(new { success = false, message = "Erro desconhecido" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                foreach (var file in files)
                    _projectImageWrap.Delete(projectID, file.Key);

                return Json(new { success = false, message = ex.GetInnerException().Message },
                            JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Partials

        [ChildActionOnly]
        [CustomOutputCache(CacheProfile = "InternalResource")]
        public PartialViewResult ProjectList(bool onlyDraft = false)
        {
            var projects = ProjectService.Repository.Query();

            if (Request.IsAuthenticated && onlyDraft)
                projects = projects.OnlyDraftProjects();
            else
                projects = projects.OnlyPublishedProjects();


            return PartialView("_Projects", projects.WithDefaultOrdered().ToList());
        }

        #endregion
    }
}