﻿//#define DebugFake

using System;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using BindSolution.App_Start;
using BindSolution.Data;
using BindSolution.Framework.Service;
using BindSolution.Infra;
using BindSolution.Mailers;
using BindSolution.Mappers;
using BindSolution.Service;
using FluentValidation;

[assembly: WebActivator.PreApplicationStartMethod(typeof(Setup), "Initialize")]

namespace BindSolution.App_Start
{
    public static class Setup
    {

        public static void Initialize()
        {
            Framework.Web.App_Start.NinjectMVC3.NinjectRegister += NinjectMVC3NinjectRegister;
        }

        static void NinjectMVC3NinjectRegister(Ninject.IKernel kernel)
        {

#if DebugFake
            kernel.Bind<IProjectRepository>().To<Data.Fake.ProjectRepository>().InSingletonScope(); /* Fake */
            kernel.Bind<IUserRepository>().To<Data.Fake.UserRepository>().InSingletonScope(); /* Fake */
            kernel.Bind<IRoleRepository>().To<Data.Fake.RoleRepository>().InSingletonScope(); /* Fake */
            kernel.Bind<IAddressRepository>().To<Fake.AddressRepository>().InSingletonScope(); /* Fake */
            kernel.Bind<IProjectImageRepository>().To<Fake.ProjectImageRepository>().InSingletonScope();  /* Fake */
#else
            kernel.Bind<BindSolutionContext>().ToSelf().InRequestScope().WithConstructorArgument("getCurrentUser", ninjet => new Func<IIdentity>(() => HttpContext.Current.User.Identity));

            kernel.Bind<IProjectRepository>().To<ProjectRepository>().InRequestScope();
            kernel.Bind<IUserRepository>().To<UserRepository>().InRequestScope();
            kernel.Bind<IRoleRepository>().To<RoleRepository>().InRequestScope();
            kernel.Bind<IAddressRepository>().To<AddressRepository>().InRequestScope();
            kernel.Bind<IProjectImageRepository>().To<ProjectImageRepository>().InRequestScope();
#endif

            kernel.Bind<IProjectService>().To<ProjectService>().InRequestScope();
            kernel.Bind<IUserService>().To<UserService>().InRequestScope();
            kernel.Bind<IRoleService>().To<RoleService>().InRequestScope();

            kernel.Bind<HttpPostedFileEnumerableStringCollectionResolver>().ToSelf().InSingletonScope();

            kernel.Bind<IProjectImageWrap>().To<ProjectImageWrap>().InSingletonScope();

            kernel.Bind<IEmailSend>().To<ContactMailer>().InRequestScope();

            //kernel.Bind<RoleProvider>().To<BindRoleProvider>().InRequestScope();
            //kernel.Bind<MembershipProvider>().To<BindUserProvider>().InRequestScope();

            Framework.Web.Setup.AutoMapper<BindSolutionAutoMapperProfiler>(kernel);

            //Validators
            AssemblyScanner.FindValidatorsInAssembly(Assembly.GetExecutingAssembly()).ForEach(
                p => kernel.Bind(p.InterfaceType).To(p.ValidatorType).InRequestScope());
        }
    }
}