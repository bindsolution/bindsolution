namespace BindSolution.ViewModel
{
    #region usings

    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    #endregion

    public class AddressViewModel
    {
        [HiddenInput]
        public Guid ID { get; private set; }

        [Display(Name = "Exibi��o")]
        public string Display { get; set; }

        [Display(Name = "Rua")]
        public string Street { get; set; }

        [Display(Name = "N�")]
        public string Number { get; set; }

        [Display(Name = "Compl.")]
        public string Complement { get; set; }

        [Display(Name = "Bairro.")]
        public string Neighborhood { get; set; }

        [Display(Name = "Cidade")]
        public string City { get; set; }

        [Display(Name = "Estado")]
        public string State { get; set; }

        [Display(Name = "Descri��o")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        [UIHint("HtmlView")]
        public string Description { get; set; }

        [Display(Name = "Latidude")]
        public double Latitude { get; set; }

        [Display(Name = "Longitude")]
        public double Longitude { get; set; }

        public AddressViewModel()
        {
            ID = Guid.NewGuid();
        }
    }
}