using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace BindSolution.ViewModel
{
    public class ProjectViewModel
    {
        [HiddenInput]
        public Guid? ID { get; set; }

        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Display(Name = "Palavra chave")]
        public string Keyword { get; set; }

        [DataType(DataType.Url)]
        [Display(Name = "Link")]
        public string WebSite { get; set; }

        [Display(Name = "Logo")]
        public HttpPostedFileBase Logo { get; set; }

        [Display(Name = "Nota")]
        [UIHint("_Rate")]
        public short Rate { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Descri��o")]
        public string Description { get; set; }

        [DataType(DataType.MultilineText)]
        [AllowHtml]
        [Display(Name = "Detalhes")]
        [UIHint("HtmlView")]
        public string Details { get; set; }

        [Display(Name = "Imagens")]
        public ICollection<HttpPostedFileBase> Images { get; set; }

        [Display(Name = "Endere�os")]
        public ICollection<AddressViewModel> Addresses { get; set; }

        [Display(Name = "Publicado")]
        public bool Published { get; set; }

        public ProjectViewModel()
        {
            ID = Guid.NewGuid();
        }
    }
}