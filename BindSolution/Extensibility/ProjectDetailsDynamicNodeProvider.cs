using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BindSolution.Data;
using MvcSiteMapProvider.Extensibility;

namespace BindSolution.Extensibility
{
    #region usings

    

    #endregion

    public class ProjectDetailsDynamicNodeProvider : DynamicNodeProviderBase
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectDetailsDynamicNodeProvider()
        {
            _projectRepository = DependencyResolver.Current.GetService<IProjectRepository>();
            //_projectRepository = projectRepository;
        }

        #region Overrides of DynamicNodeProviderBase

        public override IEnumerable<DynamicNode> GetDynamicNodeCollection()
        {
            // Build value 
            var returnValue = new List<DynamicNode>();
            var listDB = (from p in _projectRepository.Query()
                          select new { p.Name, p.Keyword, p.Description });

            const string keyFormated = "Project_{0}";

            // Create a node for each project
            foreach (var project in listDB.ToList())
            {
                var root = new DynamicNode
                               {
                                   Title = project.Name,
                                   Description = project.Description,
                                   Key = string.Format(keyFormated, project.Keyword)
                               };
                root.RouteValues.Add("url", project.Keyword);
                root.Attributes.Add("visibility", "SiteMapPathHelper,!*");
                root.Attributes.Add("dynamicParameters", project.Keyword);
                returnValue.Add(root);

                var photos = new DynamicNode
                                 {
                                     Title = "Fotos",
                                     Description = project.Description,
                                     Action = "Photos",
                                     ParentKey = string.Format(keyFormated, project.Keyword)
                                 };
                photos.RouteValues.Add("url", project.Keyword);
                photos.Attributes.Add("visibility", "SiteMapPathHelper,!*");
                photos.Attributes.Add("dynamicParameters", project.Keyword);
                returnValue.Add(photos);

                var addresses = new DynamicNode
                                {
                                    Title = "Endereços"
                                    ,
                                    Description = project.Description
                                    ,
                                    Action = "Addresses"
                                    ,
                                    ParentKey = string.Format(keyFormated, project.Keyword)
                                };
                addresses.RouteValues.Add("url", project.Keyword);
                addresses.Attributes.Add("visibility", "SiteMapPathHelper,!*");
                addresses.Attributes.Add("dynamicParameters", project.Keyword);
                returnValue.Add(addresses);

                var admin = new DynamicNode
                                {
                                    Title = "Administração"
                                    ,
                                    Description = project.Description
                                    ,
                                    Action = "Admin"
                                    ,
                                    ParentKey = string.Format(keyFormated, project.Keyword)
                                };
                admin.Attributes.Add("visibility", "SiteMapPathHelper,!*");
                admin.RouteValues.Add("url", project.Keyword);
                admin.Attributes.Add("dynamicParameters", project.Keyword);
                returnValue.Add(admin);
            }

            // Return 
            return returnValue;
        }

        #endregion
    }
}