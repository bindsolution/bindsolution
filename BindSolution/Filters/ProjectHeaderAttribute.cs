// //-----------------------------------------------------------------------
// // <copyright file="ProjectHeaderAttribute.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

namespace BindSolution.Filters
{
    #region usings

    using System;
    using System.IO;
    using System.Linq;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using Controllers;
    using Infra;
    using Service;

    #endregion

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ProjectHeaderAttribute : ActionFilterAttribute
    {
        public bool LoadAddresses { get; set; }

        public ProjectService ProjectService { get { return (ProjectService)DependencyResolver.Current.GetService<IProjectService>(); } }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var projectController = filterContext.Controller as ProjectController;
            if (projectController == null)
                throw new InvalidOperationException("It is not ProjectController!");

            var project = ProjectService.Get(filterContext.RouteData.Values["url"].ToString());
            if (project == null)
            {
                filterContext.Result = new HttpNotFoundResult("Projeto n�o encontrado");
                return;
            }

            var pathLogo = Path.Combine(string.Format(Constants.ProjectImageTumbsPathFormat, project.ID), project.Logo);
            pathLogo = filterContext.HttpContext.Server.MapPath(pathLogo);
            if (!File.Exists(pathLogo))
                throw new FileNotFoundException("Logo do projeto " + project.Name + " n�o foi encontrada!");
            var logo = new WebImage(pathLogo);

            filterContext.Controller.ViewBag.LogoImageName = Path.GetFileNameWithoutExtension(logo.FileName);

            // ReSharper disable AssignNullToNotNullAttribute
            filterContext.Controller.ViewBag.LogoImagePath = Path.Combine(string.Format(Constants.ProjectImageTumbsPathFormat, project.ID), Path.GetFileName(logo.FileName));
            // ReSharper restore AssignNullToNotNullAttribute

            filterContext.Controller.ViewBag.WebLogo = logo;

            if (filterContext.ActionParameters.Keys.Any(p => p == "project"))
                filterContext.ActionParameters["project"] = project;

            base.OnActionExecuting(filterContext);

        }
    }
}