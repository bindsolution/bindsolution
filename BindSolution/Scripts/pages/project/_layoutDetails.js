
/*
    Private functions
*/

(function() {
  var ajaxLoaderContent, loaderAboutPage, loaderAddressesPage, loaderPage, loaderPhotosPage;

  ajaxLoaderContent = function(url) {
    return $.get(url, function(data) {
      return $("#project-container").html(data);
    });
  };

  loaderPage = function(url) {
    var content, pageUrl;
    content = $("#project-menu li a[href$=\"" + url + "\"]").text();
    $("#project-container").html($("#statusTmpl").render([
      {
        message: "aguarde, carregando " + content.toLowerCase()
      }
    ]));
    $("#project-menu li.selected").removeClass("selected");
    pageUrl = url.substr(url.lastIndexOf("/") + 1, url.length);
    $("#project-menu li a[href$=\"" + pageUrl + "\"]").parent("li").addClass("selected");
    pageUrl = pageUrl.toLowerCase();
    if (pageUrl.indexOf("photos") !== -1) {
      return loaderPhotosPage(url);
    } else if (pageUrl.indexOf("addresses") !== -1) {
      return loaderAddressesPage(url);
    } else {
      return loaderAboutPage(url);
    }
  };

  loaderAboutPage = function(url) {
    window.location.hash = '';
    return ajaxLoaderContent(url);
  };

  loaderPhotosPage = function(url) {
    window.location.hash = 'Photos';
    return $.ajax({
      url: '/Scripts/pages/project-projectPhotos/js',
      dataType: "script",
      error: function(err) {
        $("#project-container").html("Ocorreu um erro ao tentar carregar a página. Por favor, tente novamente.");
        return console.log(err);
      },
      success: function() {
        return ajaxLoaderContent(url);
      }
    });
    /*
        $.getScript '/Scripts/pages/project-projectPhotos/js', () -> 
            ajaxLoaderContent url
            alert 'OK, funcionou!'
    */
  };

  loaderAddressesPage = function(url) {
    window.location.hash = 'Addresses';
    return ajaxLoaderContent(url);
  };

  /* 
      Public functions
  */

  window.projectLayoutDetailsInit = function() {
    var $shareProject, $shareProjectButton, hash;
    if (window.location.hash) {
      hash = window.location.hash.replace("#", "");
      ajaxLoaderContent(window.location.pathname + "/" + hash);
    }
    $shareProjectButton = $("#project-shared-button");
    $shareProject = $("#project-shared");
    $shareProjectButton.bind({
      mouseenter: function() {
        return $shareProjectButton.stop().hide("fade", {}, 400, function() {
          $shareProject.attr("style", "");
          return $shareProject.show("slide", 200);
        });
      }
    });
    $shareProject.bind({
      mouseleave: function() {
        return $shareProject.stop().hide("slide", {}, 400, function() {
          return $shareProjectButton.show("fade", {}, 400, function() {
            return $shareProjectButton.attr("style", "");
          });
        });
      }
    });
    return $shareProjectButton.qtip({
      position: {
        at: 'right center',
        my: 'left center'
      },
      content: {
        attr: "title"
      },
      show: {
        ready: true,
        delay: 2000,
        effect: function(offset) {
          return $(this).show("drop", 550);
        }
      },
      hide: {
        event: "click mouseenter",
        effect: function(offset) {
          return $(this).hide("fade", 150);
        }
      },
      style: {
        tip: true,
        classes: 'ui-tooltip-green ui-tooltip-shadow'
      }
    });
  };

}).call(this);
