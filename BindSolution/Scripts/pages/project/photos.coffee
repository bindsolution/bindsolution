### 
    Public functions
###
window.projectPhotosInit = ->
    projectLayoutDetailsInit()

    #Delete image
    $("ul.images .item .project-imageDelete").css "display", "none"

    #Delete image Action
    $("ul.images .item .project-imageDelete").click (e) -> 
        
        #Initialize
        e.preventDefault()

        #elementDialog = $("#project-ImageDeleteDialog")

        itemDiv = $(this).parents(".item:first")
        hrefDeleteA = itemDiv.children().not(".frame:first").attr("href")
        $.ajaxAntiForgery
            url: hrefDeleteA
            type: "POST"
            success: (data) -> 
                if data.success
                    $(".item a[href$='" + encodeURI(data.image.fileName) + "']").parent().fadeOut
                        callback: () -> 
                            $(this).remove()
                else
                    alert data.message

            error: (jqXHR) -> 
                alert jqXHR.statusText
        ###
        $.postAntiForgery hrefDeleteA, (data) -> 
            alert 'ok' if data.success
        ###     


    ##Delete image Efect
    $("ul.images .item").stop().bind
        mouseenter: ->
            $(".project-imageDelete", this).toggle("drop", 150)
        mouseleave: ->
            $(".project-imageDelete", this).fadeOut()

    #Zoom image
    $("ul.images .item a[rel=projectImage]").fancybox
        width: "75%"
        height: "25%"
        transitionIn: "elastic"
        transitionOut: "elastic"
        titlePosition: "over"
        titleFormat: formatTitle = (title, currentArray, currentIndex, currentOpts) ->
            "<div id=\"fancybox-title-over\">" + title + "</div>"

        speedIn: 350
        speedOut: 600
        overlayOpacity: 0.7
        hideOnContentClick: true