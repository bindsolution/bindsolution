###
    Private functions
###
ajaxLoaderContent = (url) ->
    $.get url, (data) ->
        $("#project-container").html data
    
loaderPage = (url) ->
    
    #Loading...
    content = $("#project-menu li a[href$=\"" + url + "\"]").text()
    $("#project-container").html $("#statusTmpl").render([ message: "aguarde, carregando " + content.toLowerCase() ])

    $("#project-menu li.selected").removeClass "selected"
    pageUrl = url.substr(url.lastIndexOf("/") + 1, url.length)
    $("#project-menu li a[href$=\"" + pageUrl + "\"]").parent("li").addClass "selected"
    
    #Page
    pageUrl = pageUrl.toLowerCase()
    if pageUrl.indexOf("photos") != -1 
        loaderPhotosPage(url)

    else if pageUrl.indexOf("addresses") != -1 
        loaderAddressesPage(url)

    else
        loaderAboutPage(url)

loaderAboutPage = (url) ->
    window.location.hash = ''
    ajaxLoaderContent url

loaderPhotosPage = (url) ->
    window.location.hash = 'Photos'
    $.ajax
        url: '/Scripts/pages/project-projectPhotos/js'
        dataType: "script"
        error: (err) ->
            $("#project-container").html "Ocorreu um erro ao tentar carregar a p�gina. Por favor, tente novamente."
            console.log(err)
        success: () -> 
            ajaxLoaderContent url
            

    ###
    $.getScript '/Scripts/pages/project-projectPhotos/js', () -> 
        ajaxLoaderContent url
        alert 'OK, funcionou!'
    ###
    

loaderAddressesPage = (url) ->
    window.location.hash = 'Addresses'
    ajaxLoaderContent url


### 
    Public functions
###
window.projectLayoutDetailsInit = ->
    if window.location.hash
        hash = window.location.hash.replace("#", "")
        ajaxLoaderContent window.location.pathname + "/" + hash

    # Share
    $shareProjectButton = $("#project-shared-button")
    $shareProject = $("#project-shared")
    $shareProjectButton.bind(
        mouseenter: () -> 
            $shareProjectButton.stop().hide("fade", {}, 400, () -> 
                    $shareProject.attr("style", "");
                    $shareProject.show("slide", 200)
            )
    )
    $shareProject.bind(
        mouseleave: () -> 
            $shareProject.stop().hide("slide", {}, 400, () -> 
                    $shareProjectButton.show("fade",{}, 400, () -> 
                        $shareProjectButton.attr("style", "");
                    )
                    
            )
    )

    $shareProjectButton.qtip
        position:
            at: 'right center'
            my: 'left center'
        content:
            attr: "title"
        show:
            ready: true
            delay: 2000
            effect: (offset) -> 
                $(this).show("drop", 550)
        hide:
            event: "click mouseenter"
            effect: (offset) -> 
                $(this).hide("fade", 150)
        style: 
            tip: true
            classes: 'ui-tooltip-green ui-tooltip-shadow'