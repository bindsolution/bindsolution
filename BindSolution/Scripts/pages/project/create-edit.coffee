### 
    Public functions
###
window.projectCreateEditInit = ->
    
    $("form").bindAjaxPost({ statusMessage: "salvando projeto..." })

    # Default values
    $("#WebSite").change( -> 

        $("img[src$='status_unknown.png']", $(this).parent()).remove()

        #Try get info in website
        $.ajax(
            url: $(this).val()
            crossDomain : true
            
            #Start loading
            beforeSend: -> 
                loader = $("#WebSiteLoader")
                if loader.length > 0
                    loader.bindLoader "stop"
                    loader.empty()
                else
                    loader = $("<div id='WebSiteLoader'>").appendTo $("#WebSite").parent()
                loader.bindLoader({radius: 5, lines: 8, width: 5})

            #Show infos for website
            success: (resp) ->
                det = $(resp).filter("meta[name|='Description']").attr "content"
                desc = $(resp).filter("meta[name|='Classification']").attr "content"
                title = $(resp).filter("title").text()
                $("#Description").effect("highlight", 1000).val(desc)
                $("#Details").wysiwyg("setContent", det)
                $("#Keyword").effect("highlight", 1000).val(formatKeyword(title))
                $("#Name").effect("highlight", 1000).val(title)
                $("#Name").focus()
            
            # Show infos for url
            error: (jqXHR, textStatus, errorThrown) -> 
                                
                currentUrl = $("#WebSite").val().match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/)
                if currentUrl and currentUrl.length > 0
                    #Warning
                    img = $("#unknownStatusTmpl").render(
                        text: "N�o foi poss�vel acessar as informa��es deste site\nPossivelmente n�o se trata de um site desenvolvido pela Bind Solution."
                    )
                    $(img).show("drop").appendTo $("#WebSite").parent()
                    
                    #Link
                    currentUrl = currentUrl[2].split(".")[0]
                    $("#Keyword").effect("highlight", 1000).val(formatKeyword(currentUrl))
                    $("#Name").effect("highlight", 1000).val(currentUrl)
                    $("#Name").focus()
            
            # Stop loader
            complete: ->
                loader = $("#WebSiteLoader")
                loader.bindLoader("stop")
                loader.remove()
        )

    )
    $("#Name").change( -> 
        currentName = formatKeyword $(this).val()
        $("#Keyword").effect("highlight", 1000).val(currentName)
        $("#Keyword").focus()
    )
    $("#Keyword").change( -> 
        currentName = formatKeyword $(this).val()
        $("#Keyword").effect("highlight", 1000).val(currentName)
    )

    # Components
    $("#Details").wysiwyg
        initialContent: ""
        controls:
            createLink:
                visible: false
            insertImage:
                visible: false
            subscript:
                visible: false
            superscript:
                visible: false
            h1:
                visible: false

    $("#Logo").bindAjaxUploader
        action: "/Project/Upload/LogoUpload"
        allowedExtensions: [ "jpg", "jpeg", "png", "gif" ]
        params:
            ProjectID: $("#ID").val()


### 
    Private functions
###
formatKeyword = (original) ->
    original = original.replace(new RegExp(" ", 'g'),"-").toLowerCase();
    encodeURIComponent(original)