(function() {

  /*
      Private functions
  */

  var centerMap, loadMap;

  loadMap = function() {
    var mapElement, optsBtnListAddresses;
    mapElement = $("#addresses-map");
    mapElement.bindGmaps();
    optsBtnListAddresses = {
      btnPos: google.maps.ControlPosition.TOP_CENTER,
      events: [
        {
          name: "change",
          "function": function() {
            return centerMap();
          }
        }, {
          name: "click",
          "function": function() {
            return centerMap();
          }
        }
      ]
    };
    mapElement.bindGmaps("addButtom", "gmapsAddressesSltTmpl", optsBtnListAddresses);
    mapElement.bindGmaps("addBindMarker");
    return $.getJSON(window.location.pathname, function(data) {
      var address, formatedAddress, latLng, optsMarker, _i, _len;
      for (_i = 0, _len = data.length; _i < _len; _i++) {
        address = data[_i];
        formatedAddress = "" + address.Street + " " + address.Number;
        if (data.Complement) {
          formatedAddress += formatedAddress + " Compl. " + address.Complement;
        }
        optsMarker = {
          title: formatedAddress,
          position: new google.maps.LatLng(address.Latitude, address.Longitude),
          infoWindOpts: {
            autoShow: true,
            opts: {
              content: $("#gmapsInfoWindowTmpl").render(address)
            }
          }
        };
        mapElement.bindGmaps("addMarker", optsMarker);
      }
      if ((data != null ? data.length : void 0) > 0) {
        latLng = new google.maps.LatLng(data[0].Latitude, data[0].Longitude);
        return $("#addresses-map").bindGmaps("center", latLng);
      }
    });
  };

  /* 
      Public functions
  */

  window.projectAddressesInit = function() {
    projectLayoutDetailsInit();
    return loadMap();
  };

  centerMap = function() {
    var lat, latLng, latLngArray, lng;
    latLngArray = $("#addresses > option:selected");
    if (latLngArray.length > 0) {
      lat = latLngArray.data("address-latitude").replace(",", ".");
      lng = latLngArray.data("address-longitude").replace(",", ".");
      latLng = new google.maps.LatLng(lat, lng);
      return $("#addresses-map").bindGmaps("center", latLng);
    }
  };

  /*
      Inline js
  */

}).call(this);
