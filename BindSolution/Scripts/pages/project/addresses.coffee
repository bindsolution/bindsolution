###
    Private functions
###

loadMap = -> 

    mapElement = $("#addresses-map");
    mapElement.bindGmaps()

    #Buttom
    optsBtnListAddresses = 
        btnPos: google.maps.ControlPosition.TOP_CENTER
        events: [
            {
                name: "change"
                function: -> centerMap()
            }
            {
                name: "click"
                function: -> centerMap()
            }
        ]
    mapElement.bindGmaps("addButtom", "gmapsAddressesSltTmpl", optsBtnListAddresses)

    #Marker
    mapElement.bindGmaps("addBindMarker")

    #Others address
    $.getJSON(window.location.pathname, (data) ->
        for address in data
            formatedAddress = "#{address.Street} #{address.Number}"
            if data.Complement
                formatedAddress += formatedAddress + " Compl. " + address.Complement
            optsMarker =
                title: formatedAddress
                position: new google.maps.LatLng(address.Latitude, address.Longitude)
                infoWindOpts: 
                    autoShow: true
                    opts: 
                        content: $("#gmapsInfoWindowTmpl").render(address)
            mapElement.bindGmaps("addMarker", optsMarker) 
        
        #Center
        if data?.length > 0
            latLng = new google.maps.LatLng(data[0].Latitude, data[0].Longitude)
            $("#addresses-map").bindGmaps("center", latLng)
        )    
        

### 
    Public functions
###
window.projectAddressesInit = ->
    projectLayoutDetailsInit()
    loadMap()

centerMap = -> 
    latLngArray = $("#addresses > option:selected")
    if latLngArray.length > 0   
        lat = latLngArray.data("address-latitude").replace(",", ".")
        lng = latLngArray.data("address-longitude").replace(",", ".")
        latLng = new google.maps.LatLng(lat, lng)
        $("#addresses-map").bindGmaps("center", latLng)


###
    Inline js
### 

