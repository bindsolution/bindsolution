(function() {

  /* 
      Public functions
  */

  var formatKeyword;

  window.projectCreateEditInit = function() {
    $("form").bindAjaxPost({
      statusMessage: "salvando projeto..."
    });
    $("#WebSite").change(function() {
      $("img[src$='status_unknown.png']", $(this).parent()).remove();
      return $.ajax({
        url: $(this).val(),
        crossDomain: true,
        beforeSend: function() {
          var loader;
          loader = $("#WebSiteLoader");
          if (loader.length > 0) {
            loader.bindLoader("stop");
            loader.empty();
          } else {
            loader = $("<div id='WebSiteLoader'>").appendTo($("#WebSite").parent());
          }
          return loader.bindLoader({
            radius: 5,
            lines: 8,
            width: 5
          });
        },
        success: function(resp) {
          var desc, det, title;
          det = $(resp).filter("meta[name|='Description']").attr("content");
          desc = $(resp).filter("meta[name|='Classification']").attr("content");
          title = $(resp).filter("title").text();
          $("#Description").effect("highlight", 1000).val(desc);
          $("#Details").wysiwyg("setContent", det);
          $("#Keyword").effect("highlight", 1000).val(formatKeyword(title));
          $("#Name").effect("highlight", 1000).val(title);
          return $("#Name").focus();
        },
        error: function(jqXHR, textStatus, errorThrown) {
          var currentUrl, img;
          currentUrl = $("#WebSite").val().match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/);
          if (currentUrl && currentUrl.length > 0) {
            img = $("#unknownStatusTmpl").render({
              text: "Não foi possível acessar as informações deste site\nPossivelmente não se trata de um site desenvolvido pela Bind Solution."
            });
            $(img).show("drop").appendTo($("#WebSite").parent());
            currentUrl = currentUrl[2].split(".")[0];
            $("#Keyword").effect("highlight", 1000).val(formatKeyword(currentUrl));
            $("#Name").effect("highlight", 1000).val(currentUrl);
            return $("#Name").focus();
          }
        },
        complete: function() {
          var loader;
          loader = $("#WebSiteLoader");
          loader.bindLoader("stop");
          return loader.remove();
        }
      });
    });
    $("#Name").change(function() {
      var currentName;
      currentName = formatKeyword($(this).val());
      $("#Keyword").effect("highlight", 1000).val(currentName);
      return $("#Keyword").focus();
    });
    $("#Keyword").change(function() {
      var currentName;
      currentName = formatKeyword($(this).val());
      return $("#Keyword").effect("highlight", 1000).val(currentName);
    });
    $("#Details").wysiwyg({
      initialContent: "",
      controls: {
        createLink: {
          visible: false
        },
        insertImage: {
          visible: false
        },
        subscript: {
          visible: false
        },
        superscript: {
          visible: false
        },
        h1: {
          visible: false
        }
      }
    });
    return $("#Logo").bindAjaxUploader({
      action: "/Project/Upload/LogoUpload",
      allowedExtensions: ["jpg", "jpeg", "png", "gif"],
      params: {
        ProjectID: $("#ID").val()
      }
    });
  };

  /* 
      Private functions
  */

  formatKeyword = function(original) {
    original = original.replace(new RegExp(" ", 'g'), "-").toLowerCase();
    return encodeURIComponent(original);
  };

}).call(this);
