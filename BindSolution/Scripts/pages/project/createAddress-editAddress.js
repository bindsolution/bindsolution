(function() {

  /* 
      Public functions
  */

  var getAddressFormated, setAddressValue;

  window.projectCreateAddressEditAddressInit = function() {
    $("form").bindAjaxPost({
      statusMessage: "salvando endereço..."
    });
    $("#State, #Street, #City, #Number, #Neighborhood").change(function() {
      if ($("#Street").val().length <= 0 || $("#Number").val().length <= 0) return;
      return new google.maps.Geocoder().geocode({
        address: getAddressFormated()
      }, function(results, status) {
        var comp, lat, lng, _i, _len, _ref;
        if (status === google.maps.GeocoderStatus.OK) {
          _ref = results[0].address_components;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            comp = _ref[_i];
            setAddressValue(comp.types[0], comp.long_name);
          }
          if (results[0].geometry.location_type === "RANGE_INTERPOLATED" || results[0].geometry.location_type === "ROOFTOP") {
            lat = results[0].geometry.location.Qa;
            lng = results[0].geometry.location.Ra;
          } else {
            lat = results[0].geometry.location.PA;
            lng = results[0].geometry.location.Qa;
          }
          $("#Latitude").val(lat);
          $("#Longitude").val(lng);
          return $("#Description").focus();
        }
      });
    });
    return $("#Description").wysiwyg({
      initialContent: "",
      controls: {
        createLink: {
          visible: false
        },
        insertTable: {
          visible: false
        },
        insertImage: {
          visible: false
        },
        subscript: {
          visible: false
        },
        superscript: {
          visible: false
        },
        h1: {
          visible: false
        },
        h2: {
          visible: false
        }
      }
    });
  };

  /* 
      Private functions
  */

  setAddressValue = function(fieldInMap, val) {
    var map;
    map = new Array();
    map["route"] = "#Street";
    map["sublocality"] = "#Neighborhood";
    map["street_number"] = "#Number";
    map["locality"] = "#City";
    map["administrative_area_level_1"] = "#State";
    map["country"] = "#Country";
    map["postal_code"] = "#PostalCode";
    return $(map[fieldInMap]).effect("highlight", 1000).val(val);
  };

  getAddressFormated = function() {
    return "" + ($("#Street").val()) + " " + ($("#Number").val()) + " - " + ($("#City").val()) + " - " + ($("#State").val());
  };

}).call(this);
