(function() {

  /*
      Private functions
  */

  var applyZoom, onCompleteUpload, onSuccessPublish;

  onSuccessPublish = function(data, status) {
    $("#publish-dialog").dialog("close");
    return $("#admin-menus .menu-container li.icon > a[href$='Publish']").fadeOut(200, function() {
      var imgEl;
      imgEl = $("img", $(this));
      imgEl.toggleClass("admin\/publish\.png admin\/unpublish\.png");
      if (imgEl.hasClass("admin\/publish\.png")) {
        return $("span", this).text("Publicar");
      } else {
        return $("span", this).text("Despublicar");
      }
    }).fadeIn(400);
  };

  applyZoom = function(elements) {
    var formatTitle;
    return elements.fancybox({
      width: "75%",
      height: "25%",
      transitionIn: "elastic",
      transitionOut: "elastic",
      titlePosition: "over",
      titleFormat: formatTitle = function(title, currentArray, currentIndex, currentOpts) {
        return "<div id=\"fancybox-title-over\">" + title + "</div>";
      },
      speedIn: 350,
      speedOut: 600,
      overlayOpacity: 0.7,
      hideOnContentClick: true
    });
  };

  onCompleteUpload = function(jsonResult) {
    if (jsonResult.success) {
      $("#photos-table tr").parent().append($("#imageLineTmpl").render(jsonResult.image));
      applyZoom($("#photos-table tr:last td.fileName a"));
      $(".qq-upload-list li .qq-upload-file:contains(" + jsonResult.image.fileName + ")").parent().remove();
      if ($(".qq-upload-list li").length === 0) {
        return $("ul.qq-upload-list").empty();
      }
    }
  };

  /* 
      Public functions
  */

  window.projectAdminInit = function() {
    projectLayoutDetailsInit();
    if ($("#Images").length > 0) {
      $("#Images").bindAjaxUploader({
        action: "/Project/Upload/ImagesUpload",
        allowedExtensions: ["jpg", "jpeg", "png", "gif", "zip"],
        multiple: true,
        onComplete: onCompleteUpload,
        params: {
          Logo: false,
          ProjectID: $('#ID').val()
        }
      });
    }
    applyZoom($("#photos-table tr td.fileName a"));
    $("<div id='publish-dialog'>").appendTo("div[id='project-admin.page']").dialog({
      autoOpen: false,
      modal: true,
      width: 550,
      closeText: "fechar",
      resizable: false,
      show: "drop",
      hide: "drop",
      buttons: [
        {
          text: "-",
          click: function() {
            return $("#publish-dialog form").bindAjaxPost("post", {}, {
              success: onSuccessPublish
            });
          }
        }
      ]
    });
    $("#admin-menus .menu-container li.icon > a[href$='Publish']").click(function(e) {
      var span, url;
      e.preventDefault();
      url = $(this).attr("href");
      span = $("span", $(this)).text();
      return $.get(url, function(data) {
        var dialog;
        dialog = $("#publish-dialog");
        dialog.empty();
        dialog.html(data);
        $("form", dialog).bindAjaxPost({
          data: {
            redirect: false
          },
          statusMessage: "salvando alterações..."
        });
        $(".ui-dialog-buttonset > button", dialog.parent()).text(span);
        dialog.dialog("option", "title", span);
        return dialog.dialog("open");
      });
    });
    $("<div id='delete-dialog'>").appendTo("div[id='project-admin.page']").dialog({
      autoOpen: false,
      modal: true,
      width: 550,
      title: "Remover projeto",
      closeText: "fechar",
      resizable: false,
      show: "drop",
      hide: "drop",
      buttons: [
        {
          text: "Remover",
          click: function() {
            return $("#delete-dialog form").bindAjaxPost("post", {}, {
              success: function() {
                return $("#delete-dialog").dialog("close");
              }
            });
          }
        }
      ]
    });
    return $("#admin-menus .menu-container li.icon > a[href$='Delete']").click(function(e) {
      var span, url;
      e.preventDefault();
      url = $(this).attr("href");
      span = $("span", $(this)).text();
      return $.get(url, function(data) {
        var dialog;
        dialog = $("#delete-dialog");
        dialog.empty();
        dialog.html(data);
        $("form", dialog).bindAjaxPost({
          data: {
            redirect: false
          },
          statusMessage: "salvando alterações..."
        });
        return dialog.dialog("open");
      });
    });
  };

}).call(this);
