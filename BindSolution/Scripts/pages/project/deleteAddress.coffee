###
    Private functions
###

loadMap = -> 

    mapElement = $("#address-map");
    lat = $("#Latitude").val().replace(",", ".")
    lng = $("#Longitude").val().replace(",", ".")
    markerCenter = new google.maps.LatLng(lat, lng)
    mapElement.bindGmaps(
        center: markerCenter
        draggable: false
        mapTypeControl: false
    )

    #Marker
    optsMarker =
        title: $("legend").text()
        position: markerCenter
    mapElement.bindGmaps("addMarker", optsMarker) 

### 
    Public functions
###
window.projectDeleteAddressInit = ->
    loadMap()
    $("form").bindAjaxPost({ statusMessage: "removendo endere�o..." })