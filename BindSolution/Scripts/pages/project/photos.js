
  /* 
      Public functions
  */

  window.projectPhotosInit = function() {
    var formatTitle;
    projectLayoutDetailsInit();
    $("ul.images .item .project-imageDelete").css("display", "none");
    $("ul.images .item .project-imageDelete").click(function(e) {
      var hrefDeleteA, itemDiv;
      e.preventDefault();
      itemDiv = $(this).parents(".item:first");
      hrefDeleteA = itemDiv.children().not(".frame:first").attr("href");
      return $.ajaxAntiForgery({
        url: hrefDeleteA,
        type: "POST",
        success: function(data) {
          if (data.success) {
            return $(".item a[href$='" + encodeURI(data.image.fileName) + "']").parent().fadeOut({
              callback: function() {
                return $(this).remove();
              }
            });
          } else {
            return alert(data.message);
          }
        },
        error: function(jqXHR) {
          return alert(jqXHR.statusText);
        }
      });
      /*
              $.postAntiForgery hrefDeleteA, (data) -> 
                  alert 'ok' if data.success
      */
    });
    $("ul.images .item").stop().bind({
      mouseenter: function() {
        return $(".project-imageDelete", this).toggle("drop", 150);
      },
      mouseleave: function() {
        return $(".project-imageDelete", this).fadeOut();
      }
    });
    return $("ul.images .item a[rel=projectImage]").fancybox({
      width: "75%",
      height: "25%",
      transitionIn: "elastic",
      transitionOut: "elastic",
      titlePosition: "over",
      titleFormat: formatTitle = function(title, currentArray, currentIndex, currentOpts) {
        return "<div id=\"fancybox-title-over\">" + title + "</div>";
      },
      speedIn: 350,
      speedOut: 600,
      overlayOpacity: 0.7,
      hideOnContentClick: true
    });
  };
