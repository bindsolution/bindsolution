(function() {

  /*
      Private functions
  */

  var loadMap;

  loadMap = function() {
    var lat, lng, mapElement, markerCenter, optsMarker;
    mapElement = $("#address-map");
    lat = $("#Latitude").val().replace(",", ".");
    lng = $("#Longitude").val().replace(",", ".");
    markerCenter = new google.maps.LatLng(lat, lng);
    mapElement.bindGmaps({
      center: markerCenter,
      draggable: false,
      mapTypeControl: false
    });
    optsMarker = {
      title: $("legend").text(),
      position: markerCenter
    };
    return mapElement.bindGmaps("addMarker", optsMarker);
  };

  /* 
      Public functions
  */

  window.projectDeleteAddressInit = function() {
    loadMap();
    return $("form").bindAjaxPost({
      statusMessage: "removendo endereço..."
    });
  };

}).call(this);
