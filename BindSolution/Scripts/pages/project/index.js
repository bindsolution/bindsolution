(function() {

  /*
      Private Functions
  */

  var onSuccessCreate, prepareDialogCreate;

  onSuccessCreate = function(data, status) {
    var strTmpl, ulElement;
    if (data.success) {
      $("#project-dialog").dialog("close");
      strTmpl = "<li>    <div class='item'>        <a href='{{=url}}' class='frame' data-tooltip-text='{{=project.Description}}' title='{{=project.Description}}'>            <div>                <img src='{{=img.relativeLogoPath}}' alt='{{=project.Name}}' width='{{=img.Width}}' height='{{=img.Height}}' />            </div>            <span class='title'>{{=project.Name}}</span>        </a>    </div></li>";
      $.template("projectItemCreatTmpl", strTmpl);
      if ($("#projects-draft .no-data").length > 0) {
        $("#projects-draft .no-data").remove();
        ulElement = $("<ul>").addClass("projects").insertAfter("#projects-draft > h3");
      } else {
        ulElement = $("#projects-draft > ul.projects");
      }
      return ulElement.append($.render(data, "projectItemCreatTmpl")).fadeIn(2600);
    }
  };

  prepareDialogCreate = function() {
    $("<div id='project-dialog'>").appendTo("div[id='project-index\.page']").dialog({
      autoOpen: false,
      modal: true,
      width: 850,
      resizable: true,
      title: "Novo projeto",
      show: "drop",
      hide: "drop",
      buttons: [
        {
          text: "Salvar",
          click: function() {
            return $("#project-dialog form").bindAjaxPost("post", {}, {
              success: onSuccessCreate
            });
          }
        }
      ]
    });
    if ($("#project-dialog").size() > 0) {
      if (document.createStyleSheet) {
        document.createStyleSheet('/Content/formsAuth/jquery.wysiwyg.css');
      } else {
        $("<link rel='stylesheet' href='/Content/formsAuth/jquery.wysiwyg.css' type='text/css'  />").insertAfter("title");
      }
    }
    $.ajax({
      url: "/Scripts/formsAuth/fileuploader.js",
      dataType: "script",
      async: false
    });
    $.ajax({
      url: "/Scripts/formsAuth/jquery.wysiwyg.js",
      dataType: "script",
      async: false
    });
    $.ajax({
      url: "http://ajax.aspnetcdn.com/ajax/mvc/3.0/jquery.validate.unobtrusive.min.js",
      dataType: "script",
      async: false
    });
    return $.ajax({
      url: "https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js",
      dataType: "script",
      async: false
    });
  };

  /* 
      Public functions
  */

  window.projectIndexInit = function() {
    return $("div[id='project-index\.page'] .menu > a[href$='Create']").click(function(e) {
      e.preventDefault();
      if ($("#project-dialog").length <= 0) prepareDialogCreate();
      return $.get($(this).attr("href"), function(data) {
        var dialog;
        dialog = $("#project-dialog");
        dialog.empty();
        dialog.html(data);
        dialog.dialog("open");
        setTimeout("projectCreateEditInit()", 350);
        return $("form", dialog).bindAjaxPost({
          data: {
            redirect: false
          },
          statusMessage: "salvando alterações..."
        });
      });
    });
  };

}).call(this);
