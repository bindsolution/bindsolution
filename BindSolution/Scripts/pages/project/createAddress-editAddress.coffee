### 
    Public functions
###
window.projectCreateAddressEditAddressInit = ->
    
    $("form").bindAjaxPost({ statusMessage: "salvando endere�o..." })

    $("#State, #Street, #City, #Number, #Neighborhood").change ->
        return if $("#Street").val().length <= 0 or $("#Number").val().length <= 0
        new google.maps.Geocoder().geocode(
            address: getAddressFormated()
            , (results, status) ->
                if status is google.maps.GeocoderStatus.OK
                    for comp in results[0].address_components
                        setAddressValue(comp.types[0], comp.long_name)

                    if results[0].geometry.location_type is "RANGE_INTERPOLATED" or results[0].geometry.location_type is "ROOFTOP"
                        lat = results[0].geometry.location.Qa
                        lng = results[0].geometry.location.Ra
                    else
                        lat = results[0].geometry.location.PA
                        lng = results[0].geometry.location.Qa
                    
                    $("#Latitude").val(lat) #.toString().replace(".", ",")
                    $("#Longitude").val(lng)
                    $("#Description").focus()
            )
            

    $("#Description").wysiwyg
        initialContent: ""
        controls:
            createLink:
                visible: false
            insertTable:
                visible: false
            insertImage:
                visible: false
            subscript:
                visible: false
            superscript:
                visible: false
            h1:
                visible: false
            h2:
                visible: false

### 
    Private functions
###

setAddressValue = (fieldInMap, val) ->
    map = new Array()
    map["route"] = "#Street"
    map["sublocality"] = "#Neighborhood"
    map["street_number"] = "#Number"
    map["locality"] = "#City"
    map["administrative_area_level_1"] = "#State"
    map["country"] = "#Country"
    map["postal_code"] = "#PostalCode"

    $(map[fieldInMap]).effect("highlight", 1000).val(val)

getAddressFormated = -> 
    "#{$("#Street").val()} #{$("#Number").val()} - #{$("#City").val()} - #{$("#State").val()}"