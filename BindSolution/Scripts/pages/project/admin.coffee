###
    Private functions
###
onSuccessPublish = (data, status ) ->
    $("#publish-dialog").dialog "close"
    $("#admin-menus .menu-container li.icon > a[href$='Publish']").fadeOut(200, () -> 
        imgEl = $("img", $(this))
        imgEl.toggleClass("admin\/publish\.png admin\/unpublish\.png")
        if (imgEl.hasClass("admin\/publish\.png"))
            $("span", this).text("Publicar")
        else
            $("span", this).text("Despublicar")

    ).fadeIn(400)

applyZoom = (elements) ->
    elements.fancybox
        width: "75%"
        height: "25%"
        transitionIn: "elastic"
        transitionOut: "elastic"
        titlePosition: "over"
        titleFormat: formatTitle = (title, currentArray, currentIndex, currentOpts) ->
            "<div id=\"fancybox-title-over\">" + title + "</div>"

        speedIn: 350
        speedOut: 600
        overlayOpacity: 0.7
        hideOnContentClick: true

onCompleteUpload = (jsonResult) ->
    if jsonResult.success
        $("#photos-table tr").parent().append $("#imageLineTmpl").render(jsonResult.image)
        applyZoom($("#photos-table tr:last td.fileName a"))
        $(".qq-upload-list li .qq-upload-file:contains(" + jsonResult.image.fileName + ")").parent().remove()
        $("ul.qq-upload-list").empty()  if $(".qq-upload-list li").length is 0


### 
    Public functions
###
window.projectAdminInit = ->
    projectLayoutDetailsInit()
    
    #Uploader               
    if $("#Images").length > 0
        $("#Images").bindAjaxUploader
            action: "/Project/Upload/ImagesUpload"
            allowedExtensions: [ "jpg", "jpeg", "png", "gif", "zip" ]
            multiple: true
            onComplete: onCompleteUpload
            params:
                Logo: false
                ProjectID: $('#ID').val()

    #Zoom image
    applyZoom($("#photos-table tr td.fileName a"))

    # Dialog for publish project
    $("<div id='publish-dialog'>")
        .appendTo("div[id='project-admin.page']")
        .dialog
            autoOpen: false
            modal: true
            width: 550
            closeText: "fechar"
            resizable: false
            show: "drop"
            hide: "drop"
            buttons: [
                    text: "-"
                    click: () -> 
                        $("#publish-dialog form").bindAjaxPost "post", {}, 
                            success: onSuccessPublish
            ]
    
    # Action for publish project            
    $("#admin-menus .menu-container li.icon > a[href$='Publish']").click (e) ->
        e.preventDefault()
        url = $(this).attr("href")
        span = $("span", $(this)).text()
        $.get url, (data) -> 
            dialog = $("#publish-dialog");
            dialog.empty()
            dialog.html data
            $("form", dialog).bindAjaxPost
                data:
                    redirect: false
                statusMessage: "salvando alterações..."
            $(".ui-dialog-buttonset > button", dialog.parent()).text(span)
            dialog.dialog "option" ,"title", span
            dialog.dialog "open"
            


    # Dialog for delete project
    $("<div id='delete-dialog'>")
        .appendTo("div[id='project-admin.page']")
        .dialog
            autoOpen: false
            modal: true
            width: 550
            title: "Remover projeto"
            closeText: "fechar"
            resizable: false
            show: "drop"
            hide: "drop"
            buttons: [
                    text: "Remover"
                    click: () -> 
                        $("#delete-dialog form").bindAjaxPost "post", {}, 
                            success: () -> $("#delete-dialog").dialog "close"
            ]
    
    # Action for delete project            
    $("#admin-menus .menu-container li.icon > a[href$='Delete']").click (e) ->
        e.preventDefault()
        url = $(this).attr("href")
        span = $("span", $(this)).text()
        $.get url, (data) -> 
            dialog = $("#delete-dialog");
            dialog.empty()
            dialog.html data
            $("form", dialog).bindAjaxPost
                data:
                    redirect: false
                statusMessage: "salvando alterações..."
            dialog.dialog "open"