(function() {

  /*
      Private functions
  */

  var loadContactGmap;

  loadContactGmap = function() {
    var mapElement, optsButtom, optsMap;
    mapElement = $("#contact-map");
    optsMap = {
      zoom: 16,
      center: new google.maps.LatLng(-19.900216289199694, -43.92509937286377),
      zoomControl: true,
      mapTypeControl: false,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL,
        position: google.maps.ControlPosition.RIGHT_CENTER
      }
    };
    mapElement.bindGmaps(optsMap);
    mapElement.bindGmaps("addBindMarker");
    optsButtom = {
      events: [
        {
          name: "click",
          "function": function() {
            return $("#contact-map").bindGmaps("center");
          }
        }
      ]
    };
    return mapElement.bindGmaps("addButtom", "btnCenterTmpl", optsButtom);
  };

  /* 
      Public functions
  */

  window.homeContactInit = function() {
    $("form").bindAjaxPost({
      statusMessage: "enviando email..."
    });
    return loadContactGmap();
  };

  /*
      Inline js
  */

}).call(this);
