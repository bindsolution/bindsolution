
  /* 
      Public functions
  */

  window.homeWhyPlusInit = function() {
    if (window.location.hash) {
      return $(window.location.hash).effect("highlight", {
        color: "#DDFBDD"
      }, 2500);
    }
  };
