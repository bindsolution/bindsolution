
/*
    Private functions
*/

(function() {
  var publicityImageChange, publicityImageSize, publicityRotate;

  publicityRotate = function() {
    return $("#publicity-happy").rotate({
      bind: {
        mouseover: function() {
          return $(this).children("img").rotate({
            animateTo: -65
          });
        },
        mouseout: function() {
          return $(this).children("img").rotate({
            animateTo: 0
          });
        }
      }
    });
  };

  publicityImageSize = function() {
    return $("#publicity-visibility").stop().bind({
      mouseenter: function() {
        return $(this).children("img").animate({
          width: "56px"
        }, 400);
      },
      mouseleave: function() {
        return $(this).children("img").animate({
          width: "48px"
        }, 400);
      }
    });
  };

  publicityImageChange = function() {
    return $("#publicity-quality").stop().bind({
      mouseenter: function() {
        return $(this).children("img").animate({
          "background-position-x": "0"
        }, 200, "linear");
      },
      mouseleave: function() {
        return $(this).children("img").animate({
          "background-position-x": "-48px"
        }, 200, "linear");
      }
    });
  };

  /* 
      Public functions
  */

  window.homeIndexInit = function() {
    $("#frame-home > div").easySlider({
      continuous: true,
      controlsShow: false,
      auto: true,
      pause: 4500,
      speed: 1200
    });
    $("#presentation").stop().bind({
      mouseenter: function() {
        return $(this).animate({
          "right": "-7px"
        }, {
          queue: false,
          duration: 250,
          easing: "easeInBack"
        });
      },
      mouseleave: function() {
        return $(this).animate({
          "right": "-96px"
        }, {
          queue: false,
          duration: 1500,
          easing: "easeInElastic"
        });
      }
    });
    $(".prezi-player").dialog({
      autoOpen: false,
      modal: true,
      width: 590,
      title: "Apresentação da Bind Solution",
      closeText: "fechar",
      resizable: false,
      show: "drop",
      hide: "drop"
    });
    $(".link-presentation").click(function(e) {
      e.preventDefault();
      $(".prezi-player").dialog("open");
      return console.log($(".prezi-player"));
    });
    $("#ourprojects-home").find(".projects > li .item > a").qtip({
      position: {
        my: "top center",
        at: "bottom center"
      },
      content: {
        attr: "title"
      },
      show: {
        effect: function(offset) {
          return $(this).slideDown(150);
        }
      },
      hide: {
        effect: function(offset) {
          return $(this).slideUp(150);
        }
      },
      style: {
        tip: true,
        classes: 'ui-tooltip-green ui-tooltip-shadow'
      }
    });
    return $("#presentation > a.link-presentation").qtip({
      position: {
        at: 'center left',
        my: 'right center'
      },
      content: {
        attr: "title"
      },
      show: {
        ready: true,
        delay: 2000,
        effect: function(offset) {
          return $(this).show("drop", 550);
        }
      },
      hide: {
        event: "click mouseenter",
        effect: function(offset) {
          return $(this).hide("fade", 550);
        }
      },
      style: {
        tip: true,
        classes: 'ui-tooltip-green ui-tooltip-shadow'
      }
    });
  };

  /*
      Inline js
  */

  yepnope({
    test: Modernizr.csstransitions && Modernizr.csstransforms,
    nope: {
      rotate: "/Scripts/commun/jQueryRotateCompressed.2.1.js"
    },
    callback: {
      rotate: function(url, result) {
        if (!result) {
          publicityRotate();
          publicityImageSize();
          return publicityImageChange();
        }
      }
    }
  });

}).call(this);
