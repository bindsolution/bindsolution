###
    Private functions
###
loadContactGmap = -> 
    #Google Maps Interaction
    mapElement = $("#contact-map")
    optsMap = 
        zoom: 16
        center: new google.maps.LatLng(-19.900216289199694, -43.92509937286377)
        zoomControl: true
        mapTypeControl: false
        zoomControlOptions:
            style: google.maps.ZoomControlStyle.SMALL
            position: google.maps.ControlPosition.RIGHT_CENTER
    mapElement.bindGmaps(optsMap)
        
    #Marker
    mapElement.bindGmaps("addBindMarker")

    #Buttom
    optsButtom = 
        events: [
            name: "click"
            function: -> $("#contact-map").bindGmaps("center")
        ]
            
    mapElement.bindGmaps("addButtom", "btnCenterTmpl", optsButtom)

### 
    Public functions
###
window.homeContactInit = ->
    #Forms
    $("form").bindAjaxPost({ statusMessage: "enviando email..." })
    loadContactGmap()


###
    Inline js
###    
