(function() {

  /*
      Private functions
  */

  var plusBusinessImageChange, plusTechImageChange;

  plusTechImageChange = function() {
    return $("#plus-tech").stop().bind({
      mouseenter: function() {
        return $(this).children(".plus-icon").animate({
          "background-position-x": "-145"
        }, 200, "linear");
      },
      mouseleave: function() {
        return $(this).children(".plus-icon").animate({
          "background-position-x": "-97"
        }, 200, "linear");
      }
    });
  };

  plusBusinessImageChange = function() {
    return $("#plus-business").stop().bind({
      mouseenter: function() {
        return $(this).children(".plus-icon").animate({
          "background-position-x": "-48"
        }, 200, "linear");
      },
      mouseleave: function() {
        return $(this).children(".plus-icon").animate({
          "background-position-x": "0"
        }, 200, "linear");
      }
    });
  };

  /* 
      Public functions
  */

  window.homePlusInit = function() {
    if (!Modernizr.csstransitions || !Modernizr.csstransforms) {
      plusTechImageChange();
      return plusBusinessImageChange();
    }
  };

}).call(this);
