###
    Private functions
###
#Effect Image change
plusTechImageChange = ->
    $("#plus-tech").stop().bind
        mouseenter: ->
            $(this).children(".plus-icon").animate
                "background-position-x": "-145"
            , 200, "linear"

        mouseleave: ->
            $(this).children(".plus-icon").animate
                "background-position-x": "-97"
            , 200, "linear"

plusBusinessImageChange = ->
    $("#plus-business").stop().bind
        mouseenter: ->
            $(this).children(".plus-icon").animate
                "background-position-x": "-48"
            , 200, "linear"

        mouseleave: ->
            $(this).children(".plus-icon").animate
                "background-position-x": "0"
            , 200, "linear"


### 
    Public functions
###
window.homePlusInit = ->
    if not Modernizr.csstransitions or not Modernizr.csstransforms
        plusTechImageChange()
        plusBusinessImageChange()