###
    Private functions
###

#Effect Image Menu rotate
publicityRotate = ->
    $("#publicity-happy").rotate bind:
        mouseover: ->
            $(this).children("img").rotate animateTo: -65

        mouseout: ->
            $(this).children("img").rotate animateTo: 0

#Effect Image rezise
publicityImageSize = ->
    $("#publicity-visibility").stop().bind
      mouseenter: ->
          $(this).children("img").animate
              width: "56px" 
          , 400

      mouseleave: ->
          $(this).children("img").animate
              width: "48px"
          , 400

#Effect Image change
publicityImageChange = ->
    $("#publicity-quality").stop().bind
        mouseenter: ->
            $(this).children("img").animate
                "background-position-x": "0"
            , 200, "linear"

        mouseleave: ->
            $(this).children("img").animate
                "background-position-x": "-48px"
            , 200, "linear"


### 
    Public functions
###
window.homeIndexInit = ->
    $("#frame-home > div").easySlider
        continuous: true
        controlsShow: false
        auto: true
        pause: 4500
        speed: 1200

    $("#presentation").stop().bind
        mouseenter: ->
            $(this).animate
                "right": "-7px"
            ,
            queue:false
            duration:250
            easing: "easeInBack"

        mouseleave: ->
            $(this).animate
                "right": "-96px"
            ,
            queue:false
            duration:1500
            easing: "easeInElastic"

    $(".prezi-player").dialog
        autoOpen: false
        modal: true
        width: 590
        title: "Apresentação da Bind Solution"
        closeText: "fechar"
        resizable: false
        show: "drop"
        hide: "drop"

    $(".link-presentation").click (e) -> 
        e.preventDefault()
        $(".prezi-player").dialog "open"
        console.log $(".prezi-player")

    $("#ourprojects-home").find(".projects > li .item > a").qtip
        position:
            my: "top center"
            at: "bottom center"
        content:
            attr: "title"
        show:
            effect: (offset) -> 
                $(this).slideDown(150)
        hide:
            effect: (offset) -> 
                $(this).slideUp(150)
        style: 
            tip: true
            classes: 'ui-tooltip-green ui-tooltip-shadow'

    $("#presentation > a.link-presentation").qtip
        position:
            at: 'center left'
            my: 'right center'
        content:
            attr: "title"
        show:
            ready: true
            delay: 2000
            effect: (offset) -> 
                $(this).show("drop", 550)
        hide:
            event: "click mouseenter"
            effect: (offset) -> 
                $(this).hide("fade", 550)
        style: 
            tip: true
            classes: 'ui-tooltip-green ui-tooltip-shadow'



###
    Inline js
### 
yepnope
    test: Modernizr.csstransitions and Modernizr.csstransforms
    nope:
        rotate: "/Scripts/commun/jQueryRotateCompressed.2.1.js"

    callback:
        rotate: (url, result) ->
            unless result
                publicityRotate()
                publicityImageSize()
                publicityImageChange()