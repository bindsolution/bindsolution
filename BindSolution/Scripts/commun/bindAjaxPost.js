(function() {

  /*
  
     Funcionamento básico: 
  
      1. div.buttons-form => Onde ficará os botões de submit
      2. Use os helpers @Html.ValidationSummary, @Html.EditorForModel() para gerar o HTML do model. Ele gera num padrão que este plugin suporta
      3. $('--PRECISA-SER-UM-FORM--').bindAjaxPost();
      4. O Json de retorno deve ter esta estrutura: 
          State = { Name : 'name-field', ErrorMessage : 'errormessage' }
          RedirectUrl = 'Url a ser redirecionada'
      5. Caso o State esteja nothing, o plugin assume que não teve erros.
                                          
      Desenvolvido pela Bind Solution
  */

  ;

  var getMessageResult, getStatus;

  (function($, window, document) {
    var Plugin, defaults, pluginName;
    pluginName = "bindAjaxPost";
    defaults = {
      dataType: "json",
      selectorErrorMessage: "span.field-validation-error",
      classErrorField: "input-validation-error",
      statusMessage: "aguarde...",
      easeEfect: "drop",
      timeEfect: 200
    };
    Plugin = (function() {

      function Plugin(form, options) {
        this.form = form;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        if (!this.options.url) this.options.url = $(this.form).attr("action");
        this.init();
      }

      Plugin.prototype.init = function() {
        return $(this.form).submit(function(e) {
          e.preventDefault();
          $("form[action$='" + ($.trim($(this).attr("action"))) + "']").bindAjaxPost("post");
          return false;
        });
      };

      Plugin.prototype.post = function(ajaxUserOptions, events) {
        var ajaxOptions, dataString, jqxhr;
        $(this.options.selectorErrorMessage).empty();
        $("." + this.options.classErrorField).removeClass(this.options.classErrorField);
        dataString = $(this.form).serialize();
        if (this.options.data) {
          dataString = dataString + ("&" + ($.param(this.options.data)));
        }
        ajaxOptions = {
          type: "POST",
          url: this.options.url,
          data: dataString,
          dataType: this.options.dataType,
          beforeSend: function() {
            var formCurrent, opts, status;
            formCurrent = $("form[action$='" + ($.trim(this.url)) + "']");
            opts = formCurrent.data("plugin_bindAjaxPost").options;
            $(formCurrent).hide(opts.easeEfect, opts.timeEfect);
            status = getStatus(formCurrent.parents(":not(.ui-effects-wrapper):first"));
            return status.bindLoader({
              text: formCurrent.data("plugin_bindAjaxPost").options.statusMessage
            });
          },
          success: function(data, textStatus) {
            var formCurrent, formParent, message, msgLine, opts, _i, _len, _ref, _results;
            if (data.RedirectUrl) {
              return window.location.href = data.RedirectUrl;
            } else {
              formCurrent = $("form[action$='" + ($.trim(this.url)) + "']");
              formParent = formCurrent.parents(":not(.ui-effects-wrapper):first");
              if (data.State) {
                opts = formCurrent.data("plugin_bindAjaxPost").options;
                formCurrent.show(opts.easeEfect, opts.timeEfect);
                return formCurrent.bindAjaxPost("showErrors", data);
              } else {
                message = getMessageResult(formParent);
                if (data.messageLines) {
                  _ref = data.messageLines;
                  _results = [];
                  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    msgLine = _ref[_i];
                    _results.push(message.append("<p>" + msgLine + "</p>"));
                  }
                  return _results;
                } else {
                  return message.append("<p>#(textStatus)</p>");
                }
              }
            }
          },
          complete: function() {
            var formCurrent;
            formCurrent = $("form[action$='" + ($.trim(this.url)) + "']");
            return $(".status", formCurrent.parents(":not(.ui-effects-wrapper):first")).remove();
          },
          error: function(response) {
            var message;
            message = getMessageResult($("form[action$='" + ($.trim(this.url)) + "']").parents(":not(.ui-effects-wrapper):first"));
            message.append("<p>" + response.statusText + ".</p>");
            return message.append("<p>Pedimos desculpa pelo transtorno, nossa equipe já foi notificada sobre o problema!</p>");
          }
        };
        if (ajaxUserOptions) {
          ajaxOptions = $.extend({}, ajaxUserOptions, ajaxOptions);
        }
        jqxhr = $.ajax(ajaxOptions);
        if ((events != null ? events.complete : void 0) != null) {
          jqxhr.complete(function() {
            return events.complete();
          });
        }
        if ((events != null ? events.success : void 0) != null) {
          jqxhr.success(function(data, textStatus) {
            events.success(data, textStatus);
            return console.log(data);
          });
        }
        if ((events != null ? events.error : void 0) != null) {
          jqxhr.error(function(response) {
            return events.error(response);
          });
        }
        return jqxhr;
      };

      Plugin.prototype.showErrors = function(response, summaryElement) {
        var dataJson, element, elementError, item, list, spanContainer, _i, _len, _ref, _results;
        if (!response || !response.State) return;
        list = summaryElement || this.getValidationSummary();
        list.html("");
        _ref = response.State;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          item = _ref[_i];
          elementError = $("span[data-valmsg-for='" + item.Name + "']");
          if (item.Name.length === 0 || elementError.length <= 0) {
            list.append("<li>" + item.ErrorMessage + "</li>");
          }
          if (this.form && item.Name.length > 0 && elementError.length > 0) {
            element = elementError.get(0);
            if (!element.hasChildNodes()) {
              $(this.form).find("*[name='" + item.Name + "']").addClass("input-validation-error");
              spanContainer = $("span[data-valmsg-for='" + item.Name + "']");
              spanContainer.removeClass("field-validation-valid").addClass("field-validation-error");
              dataJson = {
                name: item.Name,
                message: item.ErrorMessage
              };
              _results.push($(spanContainer).html($("#span-error").render(dataJson)));
            } else {
              _results.push(void 0);
            }
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };

      Plugin.prototype.getValidationSummary = function() {
        var el;
        el = $(".validation-summary-errors");
        if (el.length === 0) {
          $("fieldset", this.form).before("<ul class='validation-summary-errors'></ul>");
          el = $(".validation-summary-errors");
        }
        return el;
      };

      return Plugin;

    })();
    return $.fn[pluginName] = function(options) {
      var args;
      args = arguments;
      if (options === undefined || typeof options === "object") {
        return this.each(function() {
          if (!$.data(this, "plugin_" + pluginName)) {
            return $.data(this, "plugin_" + pluginName, new Plugin(this, options));
          }
        });
      } else if (typeof options === "string" && options[0] !== "_" && options !== "init") {
        return this.each(function() {
          var instance;
          instance = $.data(this, "plugin_" + pluginName);
          if (instance instanceof Plugin && typeof instance[options] === "function") {
            return instance[options].apply(instance, Array.prototype.slice.call(args, 1));
          }
        });
      }
    };
  })(jQuery, window, document);

  getMessageResult = function(container) {
    var message;
    message = $(".message-result", container);
    if (message.length <= 0) {
      message = $("<div class='message-result'>").appendTo(container);
    } else {
      message.empty();
    }
    return message;
  };

  getStatus = function(container) {
    var status;
    status = $(".status", container);
    if (status.length <= 0) {
      status = $("<div class='status'>").appendTo(container);
    } else {
      status.empty();
    }
    return status;
  };

}).call(this);
