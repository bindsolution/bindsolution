
/*
    Private functions
*/

(function() {
  var IsIE, LoadBlogFeeds, LoadTwitterFeeds, TwitterPrettyDate, calculateDistance, prepareDialogLogon;

  IsIE = function() {
    var a;
    a = navigator.userAgent;
    return {
      ie: a.match(/MSIE\s([^;]*)/)
    };
  };

  TwitterPrettyDate = function(a) {
    var b, c, d, day, e, hour, minuto, week;
    b = new Date();
    c = new Date(a);
    if (IsIE.ie) c = Date.parse(a.replace(/( \+)/, " UTC$1"));
    d = b - c;
    e = 1000;
    minuto = e * 60;
    hour = minuto * 60;
    day = hour * 24;
    week = day * 7;
    if (isNaN(d) || d < 0) return "";
    if (d < e * 7) return "neste momento";
    if (d < minuto) return Math.floor(d / e) + " segundos atrás";
    if (d < minuto * 2) return "aproximadamente 1 minuto atrás";
    if (d < hour) return Math.floor(d / minuto) + " minutos atrás";
    if (d < hour * 2) return "aproximadamente 1 hora atrás";
    if (d < day) return Math.floor(d / hour) + " horas atrás";
    if (d > day && d < day * 2) return "ontem";
    if (d < day * 365) {
      return Math.floor(d / day) + " dias atrás";
    } else {
      return "mais de um ano atrás";
    }
  };

  LoadBlogFeeds = function() {
    var feedBlog;
    $.views.registerTags({
      getDay: function(val) {
        return new Date(val).getDate();
      },
      getMonthName: function(val) {
        return new Date(val).toString("MMM").toLowerCase();
      }
    });
    feedBlog = new google.feeds.Feed("http://blog.bindsolution.com/rss");
    feedBlog.setNumEntries(4);
    return feedBlog.load(function(blogFeedResult) {
      if (!blogFeedResult.error) {
        return $("#blog-feed").html($("#blogEntryTmpl").render(blogFeedResult.feed.entries));
      }
    });
  };

  LoadTwitterFeeds = function() {
    $.views.registerHelpers({
      twitterPrettyDate: function(val) {
        return TwitterPrettyDate(val);
      }
    });
    return $.getJSON("https://api.twitter.com/1/statuses/user_timeline.json?include_entities=true&include_rts=false&screen_name=bindsolution&count=5&callback=?", function(twRes) {
      return $("#twitter-feed").html($("#twitterEntryTmpl").render(twRes));
    });
  };

  prepareDialogLogon = function() {
    $("<div id='logon-dialog'>").appendTo("div[id='wrap']").dialog({
      autoOpen: false,
      modal: true,
      width: 550,
      resizable: false,
      title: "Log In",
      show: "drop",
      hide: "drop",
      buttons: [
        {
          text: "Log In",
          click: function() {
            return $("#logon-dialog form").bindAjaxPost("post");
          }
        }
      ]
    });
    if ($("#logon-dialog").size() > 0) {
      if (document.createStyleSheet) {
        document.createStyleSheet('/Content/pages/account/logon.css');
      } else {
        $("<link rel='stylesheet' href='/Content/pages/account/logon.css' type='text/css'  />").insertAfter("title");
      }
    }
    $.ajax({
      url: "http://ajax.aspnetcdn.com/ajax/mvc/3.0/jquery.validate.unobtrusive.min.js",
      dataType: "script",
      async: false
    });
    return $.ajax({
      url: "https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js",
      dataType: "script",
      async: false
    });
  };

  calculateDistance = function(elem, mouseX, mouseY) {
    return Math.floor(Math.sqrt(Math.pow(mouseX - (elem.offset().left + (elem.width() / 2)), 2) + Math.pow(mouseY - (elem.offset().top + (elem.height() / 2)), 2)));
  };

  /*
      Inline js
  */

  google.load("feeds", "1", {
    callback: LoadBlogFeeds
  });

  LoadTwitterFeeds();

  twttr.anywhere(function(T) {
    return T("#twitter-feed").hovercards();
  });

  /*
      jQuery
  */

  $(function() {
    /* ALL Pages
    */
    var $current, $map, $path;
    $("[data-tooltip-text]").qtip({
      position: {
        my: "bottom right",
        at: "top left"
      },
      content: {
        attr: "data-tooltip-text"
      },
      show: {
        effect: function(offset) {
          return $(this).toggle("drop", 150);
        }
      },
      hide: {
        effect: function(offset) {
          return $(this).hide("fade", 250);
        }
      },
      style: {
        tip: true,
        classes: 'ui-tooltip-green ui-tooltip-shadow'
      }
    });
    $("#top-menu li > a[href*='/Account/LogOn']:first").click(function(e) {
      e.preventDefault();
      if ($("#logon-dialog").length <= 0) prepareDialogLogon();
      return $.get($(this).attr("href"), function(data) {
        var dialog;
        dialog = $("#logon-dialog");
        dialog.empty();
        dialog.html(data);
        dialog.dialog("open");
        return $("form", dialog).bindAjaxPost({
          statusMessage: "aguarde..."
        });
      });
    });
    $map = $("#sitemap-path");
    $current = $map.find(".current");
    $path = $map.find(".path");
    $current.on({
      mouseenter: function() {
        return $current.stop().hide("blind", {}, 200, function() {
          return $path.show("blind", 200);
        });
      }
    });
    $path.on({
      mouseleave: function() {
        return $path.stop().hide("blind", {}, 200, function() {
          return $current.show("blind", 200);
        });
      }
    });
    if (document.getElementById("home-index.page")) {
      return homeIndexInit();
    } else if (document.getElementById("home-contact.page")) {
      return homeContactInit();
    } else if (document.getElementById("home-plus.page")) {
      return homePlusInit();
    } else if (document.getElementById("home-plusbusiness.page") || document.getElementById("home-plustech.page") || document.getElementById("home-why.page")) {
      return homeWhyPlusInit();
    } else if (document.getElementById("project-index.page")) {
      return projectIndexInit();
    } else if (document.getElementById("project-create.page") || document.getElementById("project-edit.page")) {
      return projectCreateEditInit();
    } else if (document.getElementById("project-delete.page")) {
      return projectDeleteInit();
    } else if (document.getElementById("project-about.page")) {
      return projectAboutInit();
    } else if (document.getElementById("project-photos.page")) {
      return projectPhotosInit();
    } else if (document.getElementById("project-admin.page")) {
      return projectAdminInit();
    } else if (document.getElementById("project-addresses.page")) {
      return projectAddressesInit();
    } else if (document.getElementById("project-createaddress.page") || document.getElementById("project-editaddress.page")) {
      return projectCreateAddressEditAddressInit();
    } else if (document.getElementById("project-deleteaddress.page")) {
      return projectDeleteAddressInit();
    }
  });

}).call(this);
