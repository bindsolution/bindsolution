(($) ->
  $.getAntiForgeryToken = (tokenWindow, appPath) ->
    tokenWindow = (if tokenWindow and typeof tokenWindow is typeof window then tokenWindow else window)
    appPath = (if appPath and typeof appPath is "string" then "_" + appPath.toString() else "")
    tokenName = "__RequestVerificationToken" + appPath
    inputElements = tokenWindow.document.getElementsByTagName("input")
    i = 0

    while i < inputElements.length
      inputElement = inputElements[i]
      if inputElement.type is "hidden" and inputElement.name is tokenName
        return (
          name: tokenName
          value: inputElement.value
        )
      i++

  $.appendAntiForgeryToken = (data, token) ->
    data = $.param(data)  if data and typeof data isnt "string"
    token = (if token then token else $.getAntiForgeryToken())
    data = (if data then data + "&" else "")
    (if token then data + encodeURIComponent(token.name) + "=" + encodeURIComponent(token.value) else data)
    #(if token then data + token.name + "=" + token.value else data)

  $.postAntiForgery = (url, data, callback, type) ->
    $.post url, $.appendAntiForgeryToken(data), callback, type

  $.ajaxAntiForgery = (settings) ->
    token = (if settings.token then settings.token else $.getAntiForgeryToken(settings.tokenWindow, settings.appPath))
    settings.data = $.appendAntiForgeryToken(settings.data, token)
    $.ajax settings
) jQuery