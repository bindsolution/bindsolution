String::ToJSON = ->
    href = this
    qStr = href.replace(/(.*?\?)/, "")
    qArr = qStr.split("&")
    stack = {}
    for i of qArr
        a = qArr[i].split("=")
        name = a[0]
        value = (if isNaN(a[1]) then a[1] else parseFloat(a[1]))
        if name.match(/(.*?)\[(.*?)]/)
            name = RegExp.$1
            name2 = RegExp.$2
            if name2
                stack[name] = {}  unless name of stack
                stack[name][name2] = value
            else
                stack[name] = []  unless name of stack
                stack[name].push value
        else
            stack[name] = value
    stack

String::ToJSONRegex = ->
    o = {}
    this.replace new RegExp("([^?=&]+)(=([^&]*))?", "g"), ($0, $1, $2, $3) ->
        o[$1] = $3

    o

String::format = ->
    args = arguments
    @replace /{(\d+)}/g, (match, number) ->
        (if typeof args[number] isnt `undefined` then args[number] else match)


((jQuery) ->
  getScript = jQuery.getScript
  jQuery.getScript = (resources, callback) ->
    length = resources.length
    handler = ->
      counter++

    deferreds = []
    counter = 0
    idx = 0
    while idx < length
      deferreds.push getScript(resources[idx], handler)
      idx++
    jQuery.when.apply(null, deferreds).then ->
      callback and callback()
) jQuery