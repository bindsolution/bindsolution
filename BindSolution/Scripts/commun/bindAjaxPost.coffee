###

   Funcionamento b�sico: 

    1. div.buttons-form => Onde ficar� os bot�es de submit
    2. Use os helpers @Html.ValidationSummary, @Html.EditorForModel() para gerar o HTML do model. Ele gera num padr�o que este plugin suporta
    3. $('--PRECISA-SER-UM-FORM--').bindAjaxPost();
    4. O Json de retorno deve ter esta estrutura: 
        State = { Name : 'name-field', ErrorMessage : 'errormessage' }
        RedirectUrl = 'Url a ser redirecionada'
    5. Caso o State esteja nothing, o plugin assume que n�o teve erros.
                                        
    Desenvolvido pela Bind Solution

###
#  Project: Ajax post
#  Description: 
#  Author: Bind Solution

# the semi-colon before function invocation is a safety net against concatenated
# scripts and/or other plugins which may not be closed properly.
``

(($, window, document) ->

    pluginName = "bindAjaxPost"
    
    # Defaults
    defaults =
      dataType: "json"
      selectorErrorMessage: "span.field-validation-error"
      classErrorField: "input-validation-error"
      statusMessage: "aguarde..."
      easeEfect: "drop"
      timeEfect: 200

    class Plugin
        # The actual plugin constructor
        constructor: (@form, options) ->
            @options = $.extend {}, defaults, options

            @_defaults = defaults
            @_name = pluginName
            @options.url = $(@form).attr("action") unless @options.url

            @init()


        # Init
        init: -> 
            $(@form).submit (e) ->
                e.preventDefault()
                $("form[action$='#{$.trim($(this).attr("action"))}']").bindAjaxPost("post")
                false
        # Post
        post: (ajaxUserOptions, events)-> 
            
            # Errors
            $(@options.selectorErrorMessage).empty()
            $(".#{@options.classErrorField}").removeClass @options.classErrorField

            # Default options
            dataString = $(@form).serialize()
            dataString = dataString + "&#{$.param(@options.data)}" if @options.data
            ajaxOptions = 
                type: "POST"
                url: @options.url
                data: dataString
                dataType: @options.dataType

                beforeSend: ->
                    formCurrent = $("form[action$='#{$.trim(@url)}']")
                    opts = formCurrent.data("plugin_bindAjaxPost").options
                    $(formCurrent).hide(opts.easeEfect, opts.timeEfect)
                    status = getStatus(formCurrent.parents(":not(.ui-effects-wrapper):first"))
                    status.bindLoader({ text: formCurrent.data("plugin_bindAjaxPost").options.statusMessage })
                    
                success: (data, textStatus) ->
                    if data.RedirectUrl
                        window.location.href = data.RedirectUrl
                    else
                        #Stop progress
                        formCurrent = $("form[action$='#{$.trim(@url)}']");
                        formParent = formCurrent.parents(":not(.ui-effects-wrapper):first")
                        
                        if data.State # Error
                            opts = formCurrent.data("plugin_bindAjaxPost").options
                            formCurrent.show(opts.easeEfect, opts.timeEfect)
                            formCurrent.bindAjaxPost("showErrors", data)
                        else #Success!
                            message = getMessageResult(formParent)

                            # Add msgs
                            if data.messageLines
                                for msgLine in data.messageLines
                                    message.append "<p>#{msgLine}</p>"
                            else
                                message.append "<p>#(textStatus)</p>"

                complete: -> 
                    formCurrent = $("form[action$='" + ($.trim(this.url)) + "']")
                    $(".status", formCurrent.parents(":not(.ui-effects-wrapper):first")).remove()

                error: (response) ->
                    message = getMessageResult($("form[action$='#{$.trim(@url)}']").parents(":not(.ui-effects-wrapper):first"))
                    message.append "<p>#{response.statusText}.</p>"
                    message.append "<p>Pedimos desculpa pelo transtorno, nossa equipe j� foi notificada sobre o problema!</p>"

            # Merge default and user options
            ajaxOptions = $.extend {}, ajaxUserOptions, ajaxOptions if ajaxUserOptions

            # Call request
            jqxhr = $.ajax(ajaxOptions)

            #Others events
            if events?.complete?
                jqxhr.complete -> events.complete() 
            if events?.success?
                jqxhr.success (data, textStatus) -> 
                    events.success(data, textStatus) 
                    console.log data
            if events?.error?
                jqxhr.error (response) -> events.error(response) 

            jqxhr

        # Show errors
        showErrors: (response, summaryElement) -> 
            return  if not response or not response.State
            list = summaryElement or @getValidationSummary()
            list.html ""

            for item in response.State
                elementError = $("span[data-valmsg-for='" + item.Name + "']")
                list.append "<li>" + item.ErrorMessage + "</li>"  if item.Name.length is 0 or elementError.length <= 0
                if @form and item.Name.length > 0 and elementError.length > 0
                    element = elementError.get(0)
                    unless element.hasChildNodes()
                        $(@form).find("*[name='" + item.Name + "']").addClass "input-validation-error"
                        spanContainer = $("span[data-valmsg-for='" + item.Name + "']")
                        spanContainer.removeClass("field-validation-valid").addClass "field-validation-error"
                        dataJson =
                            name: item.Name
                            message: item.ErrorMessage
                            
                        $(spanContainer).html $("#span-error").render(dataJson)        
        
        # Get validation summary
        getValidationSummary: () -> 
            el = $(".validation-summary-errors")
            if el.length is 0
                $("fieldset", @form).before "<ul class='validation-summary-errors'></ul>"
                el = $(".validation-summary-errors")
            el

    $.fn[pluginName] = (options) ->
        args = arguments;
        if options is `undefined` or typeof options is "object"
            @each ->
                $.data this, "plugin_#{pluginName}", new Plugin(this, options)  unless $.data(this, "plugin_#{pluginName}")
        else if typeof options is "string" and options[0] isnt "_" and options isnt "init"
            @each ->
                instance = $.data(this, "plugin_#{pluginName}")
                instance[options].apply instance, Array::slice.call(args, 1)  if instance instanceof Plugin and typeof instance[options] is "function"


)(jQuery, window, document)


getMessageResult = (container) -> 
    message = $(".message-result", container)
    if message.length <= 0 
        message = $("<div class='message-result'>").appendTo container
    else
        message.empty()
    
    message

getStatus = (container) -> 
    status = $(".status", container)
    if status.length <= 0 
        status = $("<div class='status'>").appendTo container
    else
        status.empty()
    
    status