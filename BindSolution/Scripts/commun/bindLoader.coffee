###
    Desenvolvido pela Bind Solution
###
#  Project: Loader
#  Description: 
#  Author: Bind Solution

# the semi-colon before function invocation is a safety net against concatenated
# scripts and/or other plugins which may not be closed properly.
``

(($, window, document) ->
    
    # Defaults
    pluginName = "bindLoader"
    defaults =
        autoStart: true
        lines: 12
        length: 0
        width: 6
        radius: 9
        color: "#6AA84F"
        speed: 1
        trail: 50
        shadow: false
        classText: "status-text"

    class Plugin
        # The actual plugin constructor
        constructor: (@element, options) ->
            @options = $.extend {}, defaults, options

            @_defaults = defaults
            @_name = pluginName

            @init()


        # Init
        init: -> 
            @spin = new Spinner(@options).spin()
            if @options.autoStart
                @start()

        # Stop animation
        stop:  -> 
            @spin.stop()
            $(@element).empty()

        # Start animation
        start: () -> 
            $(@element).empty()
            @element.appendChild(@spin.el)
            @text @options.text if @options.text
            $(@element).show()

        # Set text
        text: (text) -> 
            $("<div class=#{@options.classText}>").appendTo($(@element)).text(text).css("line-height", "0").css("text-indent", "24px") 


    $.fn[pluginName] = (options) ->
        args = arguments;
        if options is `undefined` or typeof options is "object"
            @each ->
                $.data this, "plugin_#{pluginName}", new Plugin(this, options)  unless $.data(this, "plugin_#{pluginName}")
        else if typeof options is "string" and options[0] isnt "_" and options isnt "init"
            @each ->
                instance = $.data(this, "plugin_#{pluginName}")
                instance[options].apply instance, Array::slice.call(args, 1)  if instance instanceof Plugin and typeof instance[options] is "function"


)(jQuery, window, document) 