
  /*
      Desenvolvido pela Bind Solution
  */

  ;

  (function($, window, document) {
    var Plugin, defaults, pluginName;
    if (!((typeof google !== "undefined" && google !== null ? google.maps : void 0) != null)) {
      return;
    }
    pluginName = "bindGmaps";
    defaults = {
      events: [],
      zoom: 16,
      disableDefaultUI: true,
      zoomControl: true,
      center: new google.maps.LatLng(-19.902624, -43.925399),
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.DEFAULT,
        position: google.maps.ControlPosition.RIGHT_CENTER
      },
      mapTypeControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.TOP_LEFT
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    Plugin = (function() {

      function Plugin(element, options) {
        this.element = element;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
      }

      Plugin.prototype.init = function() {
        var event, _i, _len, _ref, _results;
        this.map = new google.maps.Map(this.element, this.options);
        if (this.options.events) {
          _ref = this.options.events;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            event = _ref[_i];
            _results.push(google.maps.event.addListener(this.map, event.name, event["function"]));
          }
          return _results;
        }
      };

      Plugin.prototype.addButtom = function(buttomTmplId, opts) {
        var btn, btnElement, defBtn, event, _i, _len, _ref;
        defBtn = {
          btnPos: google.maps.ControlPosition.TOP_RIGHT,
          events: [],
          data: {},
          className: "gmapButtom"
        };
        opts = $.extend({}, defBtn, opts);
        btn = $("<div class='" + opts.className + "'>");
        btn.html($("#" + buttomTmplId).render(opts.data));
        btnElement = btn.get(0);
        this.map.controls[opts.btnPos].push(btnElement);
        if (opts.events) {
          _ref = opts.events;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            event = _ref[_i];
            google.maps.event.addDomListener(btnElement, event.name, event["function"]);
          }
        }
        $("a", btn).click(function(e) {
          return e.preventDefault();
        });
        return btn;
      };

      Plugin.prototype.addMarker = function(opts) {
        var defMkr, event, infowindow, marker, _i, _len, _ref;
        defMkr = {
          position: this.options.center,
          map: this.map,
          animation: google.maps.Animation.DROP,
          icon: "/Images/blank-marker.png",
          infoWindOpts: {
            autoShow: false,
            opts: {},
            events: []
          }
        };
        opts = $.extend({}, defMkr, opts);
        marker = new google.maps.Marker(opts);
        if (opts.infoWindOpts) {
          infowindow = new google.maps.InfoWindow(opts.infoWindOpts.opts);
          google.maps.event.addListener(marker, "click", function() {
            return infowindow.open(this.map, marker);
          });
          if (opts.infoWindOpts.events) {
            _ref = opts.infoWindOpts.events;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              event = _ref[_i];
              google.maps.event.addListener(marker, event.name, event["function"]);
            }
          }
          if (opts.infoWindOpts.autoShow) infowindow.open(this.map, marker);
        }
        return marker;
      };

      Plugin.prototype.center = function(latlng) {
        if (latlng) {
          return this.map.setCenter(latlng);
        } else {
          return this.map.setCenter(this.options.center);
        }
      };

      Plugin.prototype.addBindMarker = function() {
        var contentString, optsMarker;
        contentString = "<h2>bind solution</h2><p>Desenvolvimento de sites com foco na interação!</p><a href=\"mailto:contato@bindsolution.com\" title=\"Email bind\">contato@bindsolution.com</a>";
        optsMarker = {
          icon: "/Images/bind-marker.png",
          title: "bindsolution",
          infoWindOpts: {
            opts: {
              content: contentString
            }
          }
        };
        return this.addMarker(optsMarker);
      };

      return Plugin;

    })();
    return $.fn[pluginName] = function(options) {
      var args;
      args = arguments;
      if (options === undefined || typeof options === "object") {
        return this.each(function() {
          if (!$.data(this, "plugin_" + pluginName)) {
            return $.data(this, "plugin_" + pluginName, new Plugin(this, options));
          }
        });
      } else if (typeof options === "string" && options[0] !== "_" && options !== "init") {
        return this.each(function() {
          var instance;
          instance = $.data(this, "plugin_" + pluginName);
          if (instance instanceof Plugin && typeof instance[options] === "function") {
            return instance[options].apply(instance, Array.prototype.slice.call(args, 1));
          }
        });
      }
    };
  })(jQuery, window, document);
