###
    Private functions
###
IsIE = ->
    a = navigator.userAgent
    ie: a.match(/MSIE\s([^;]*)/)

TwitterPrettyDate = (a) ->
    b = new Date()
    c = new Date(a)
    c = Date.parse(a.replace(/( \+)/, " UTC$1"))  if IsIE.ie
    d = b - c
    e = 1000
    minuto = e * 60
    hour = minuto * 60
    day = hour * 24
    week = day * 7
    return ""  if isNaN(d) or d < 0
    return "neste momento"  if d < e * 7
    return Math.floor(d / e) + " segundos atr�s"  if d < minuto
    return "aproximadamente 1 minuto atr�s"  if d < minuto * 2
    return Math.floor(d / minuto) + " minutos atr�s"  if d < hour
    return "aproximadamente 1 hora atr�s"  if d < hour * 2
    return Math.floor(d / hour) + " horas atr�s"  if d < day
    return "ontem"  if d > day and d < day * 2
    if d < day * 365
        Math.floor(d / day) + " dias atr�s"
    else
        "mais de um ano atr�s"

#Load blog Feeds
LoadBlogFeeds = ->
  
    # Template
    $.views.registerTags
        getDay: (val) ->
          new Date(val).getDate()
        getMonthName: (val) ->
          new Date(val).toString("MMM").toLowerCase()

    # Feed
    feedBlog = new google.feeds.Feed("http://blog.bindsolution.com/rss")
    feedBlog.setNumEntries(4);
    feedBlog.load (blogFeedResult) ->
        unless blogFeedResult.error
            $("#blog-feed").html $("#blogEntryTmpl").render(blogFeedResult.feed.entries)

#Load Twitter Feeds
LoadTwitterFeeds = ->

    # Template
    $.views.registerHelpers twitterPrettyDate: (val) ->
        TwitterPrettyDate val
    # Twitters
    $.getJSON "https://api.twitter.com/1/statuses/user_timeline.json?include_entities=true&include_rts=false&screen_name=bindsolution&count=5&callback=?", (twRes) -> 
        $("#twitter-feed").html $("#twitterEntryTmpl").render(twRes)
    

# LogOn
prepareDialogLogon = () ->
    # Create dialog
    $("<div id='logon-dialog'>")
        .appendTo("div[id='wrap']")
        .dialog
            autoOpen: false
            modal: true
            width: 550
            resizable: false
            title: "Log In"
            show: "drop"
            hide: "drop"
            buttons: [
                    text: "Log In"
                    click: () -> 
                        $("#logon-dialog form").bindAjaxPost "post"
            ]

    # Load stylesheets
    if $("#logon-dialog").size() > 0
        if (document.createStyleSheet)
            document.createStyleSheet('/Content/pages/account/logon.css');
        else
            $("<link rel='stylesheet' href='/Content/pages/account/logon.css' type='text/css'  />").insertAfter("title")    

    # Load scripts
    $.ajax
        url: "http://ajax.aspnetcdn.com/ajax/mvc/3.0/jquery.validate.unobtrusive.min.js"
        dataType: "script"
        async: false
    $.ajax
        url: "https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"
        dataType: "script"
        async: false

calculateDistance = (elem, mouseX, mouseY) ->
    Math.floor Math.sqrt(Math.pow(mouseX - (elem.offset().left + (elem.width() / 2)), 2) + Math.pow(mouseY - (elem.offset().top + (elem.height() / 2)), 2))

###
    Inline js
###
google.load "feeds", "1",
     callback: LoadBlogFeeds
LoadTwitterFeeds()
twttr.anywhere (T) ->
    T("#twitter-feed").hovercards()


###
    jQuery
###
$ ->
    ### ALL Pages ###
    $("[data-tooltip-text]").qtip
        position:
            my: "bottom right"
            at: "top left"
        content:
            attr: "data-tooltip-text"
        show:
            effect: (offset) -> 
                $(this).toggle("drop", 150)
        hide:
            effect: (offset) -> 
                $(this).hide("fade", 250)
        style: 
            tip: true
            classes: 'ui-tooltip-green ui-tooltip-shadow'

    $("#top-menu li > a[href*='/Account/LogOn']:first").click( (e) ->
        e.preventDefault()
        if $("#logon-dialog").length <= 0
            prepareDialogLogon()

        $.get $(this).attr("href"), (data) -> 
            # HTML Data
            dialog = $("#logon-dialog");
            dialog.empty()
            dialog.html data

            # Execute default scripts
            dialog.dialog "open"

            $("form", dialog).bindAjaxPost
                statusMessage: "aguarde..."
    )

    # Sitemap
    $map = $("#sitemap-path")
    $current = $map.find(".current")
    $path = $map.find(".path")
    $current.on(
        mouseenter: () -> $current.stop().hide("blind", {}, 200, () -> $path.show("blind", 200))
    )
    $path.on(
        mouseleave: () -> $path.stop().hide("blind", {}, 200, () -> $current.show("blind", 200))
    )

    if (document.getElementById("home-index.page")) # Index.cshtml | ### Home controller ###
        homeIndexInit()
    else if (document.getElementById("home-contact.page")) # Contact.cshtml 
        homeContactInit()
    else if (document.getElementById("home-plus.page")) # Plus.cshtml 
        homePlusInit() 
    else if (document.getElementById("home-plusbusiness.page") or document.getElementById("home-plustech.page") or document.getElementById("home-why.page")) # plusBusiness.cshtml  | plusTech.cshtml  | Why.cshtml 
        homeWhyPlusInit()
    else if (document.getElementById("project-index.page")) # Index.cshtml | ### Project controller ###
        projectIndexInit()
    else if (document.getElementById("project-create.page") or document.getElementById("project-edit.page") ) # Create.cshtml | Edit.cshtml
        projectCreateEditInit()
    else if document.getElementById("project-delete.page") # Delete.cshtml
        projectDeleteInit()   
    else if (document.getElementById("project-about.page")) # Photos.cshtml
        projectAboutInit()
    else if (document.getElementById("project-photos.page")) # Photos.cshtml
        projectPhotosInit()
    else if (document.getElementById("project-admin.page")) # Admin.cshtml
        projectAdminInit()
    else if (document.getElementById("project-addresses.page")) # Addresses.cshtml
        projectAddressesInit()        
    else if (document.getElementById("project-createaddress.page") or document.getElementById("project-editaddress.page") ) # CreateAddress.cshtml | EditAddress.cshtml
        projectCreateAddressEditAddressInit()        
    else if document.getElementById("project-deleteaddress.page") # DeleteAddress.cshtml
        projectDeleteAddressInit()   
