
  String.prototype.ToJSON = function() {
    var a, href, i, name, name2, qArr, qStr, stack, value;
    href = this;
    qStr = href.replace(/(.*?\?)/, "");
    qArr = qStr.split("&");
    stack = {};
    for (i in qArr) {
      a = qArr[i].split("=");
      name = a[0];
      value = (isNaN(a[1]) ? a[1] : parseFloat(a[1]));
      if (name.match(/(.*?)\[(.*?)]/)) {
        name = RegExp.$1;
        name2 = RegExp.$2;
        if (name2) {
          if (!(name in stack)) stack[name] = {};
          stack[name][name2] = value;
        } else {
          if (!(name in stack)) stack[name] = [];
          stack[name].push(value);
        }
      } else {
        stack[name] = value;
      }
    }
    return stack;
  };

  String.prototype.ToJSONRegex = function() {
    var o;
    o = {};
    this.replace(new RegExp("([^?=&]+)(=([^&]*))?", "g"), function($0, $1, $2, $3) {
      return o[$1] = $3;
    });
    return o;
  };

  String.prototype.format = function() {
    var args;
    args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) {
      if (typeof args[number] !== undefined) {
        return args[number];
      } else {
        return match;
      }
    });
  };

  (function(jQuery) {
    var getScript;
    getScript = jQuery.getScript;
    return jQuery.getScript = function(resources, callback) {
      var counter, deferreds, handler, idx, length;
      length = resources.length;
      handler = function() {
        return counter++;
      };
      deferreds = [];
      counter = 0;
      idx = 0;
      while (idx < length) {
        deferreds.push(getScript(resources[idx], handler));
        idx++;
      }
      return jQuery.when.apply(null, deferreds).then(function() {
        return callback && callback();
      });
    };
  })(jQuery);
