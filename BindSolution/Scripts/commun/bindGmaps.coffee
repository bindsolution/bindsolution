###
    Desenvolvido pela Bind Solution
###
#http://code.google.com/apis/maps/documentation/javascript/examples/directions-panel.html
#  Samples: 
#    1. http://frederictonug.net/jquery-plugin-development-with-the-jquery-boilerplate
#    2. http://markdalgleish.com/2011/05/creating-highly-configurable-jquery-plugins/
#    3. http://jqueryboilerplate.com/
#  Project: API Google Maps
#  Description: 
#  Author: Bind Solution

# the semi-colon before function invocation is a safety net against concatenated
# scripts and/or other plugins which may not be closed properly.
``

(($, window, document) ->
    
    return if not google?.maps?

    # Defaults
    pluginName = "bindGmaps"
    defaults =
        events: []
        zoom: 16
        disableDefaultUI: true
        zoomControl: true
        center: new google.maps.LatLng(-19.902624, -43.925399)
        zoomControlOptions:
            style: google.maps.ZoomControlStyle.DEFAULT 
            position: google.maps.ControlPosition.RIGHT_CENTER
        mapTypeControl: true
        mapTypeControlOptions: 
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU 
            position: google.maps.ControlPosition.TOP_LEFT
        mapTypeId: google.maps.MapTypeId.ROADMAP

    class Plugin
        # The actual plugin constructor
        constructor: (@element, options) ->
            @options = $.extend {}, defaults, options

            @_defaults = defaults
            @_name = pluginName

            @init()


        #Init
        init: -> 
            @map = new google.maps.Map(@element, @options)
            if @options.events
                for event in @options.events
                    google.maps.event.addListener @map, event.name, event.function


        #Create new buttom in gmap
        addButtom: (buttomTmplId, opts) -> 
            #Options
            defBtn =
                btnPos: google.maps.ControlPosition.TOP_RIGHT
                events: []
                data: {}
                className: "gmapButtom"
            opts = $.extend {}, defBtn, opts

            #Buttom
            btn =  $("<div class='#{opts.className}'>")
            btn.html $("##{buttomTmplId}").render(opts.data)
            btnElement = btn.get(0)
            @map.controls[opts.btnPos].push btnElement

            #Events
            if opts.events
                for event in opts.events
                    google.maps.event.addDomListener btnElement, event.name, event.function

            $("a", btn).click (e) -> e.preventDefault();
            btn
        
        #Create new maker in gmap
        addMarker: (opts) ->
            #Options
            defMkr =
                position: @options.center
                map: @map
                animation: google.maps.Animation.DROP,
                icon: "/Images/blank-marker.png"
                infoWindOpts:
                    autoShow: false
                    opts: {}
                    events: []
            opts = $.extend {}, defMkr, opts

            #Create marker
            marker = new google.maps.Marker opts
            
            #Events info window
            if opts.infoWindOpts
                infowindow = new google.maps.InfoWindow opts.infoWindOpts.opts
                google.maps.event.addListener marker, "click", -> 
                    infowindow.open @map, marker
                if opts.infoWindOpts.events
                    for event in opts.infoWindOpts.events
                        google.maps.event.addListener marker, event.name, event.function
                if opts.infoWindOpts.autoShow
                    infowindow.open @map, marker
            marker

        #Center map
        center: (latlng) -> 
            if latlng
                @map.setCenter latlng
            else
                @map.setCenter @options.center


        # Marker bind
        addBindMarker: () -> 

            contentString = "<h2>bind solution</h2>
<p>Desenvolvimento de sites com foco na intera��o!</p>
<a href=\"mailto:contato@bindsolution.com\" title=\"Email bind\">contato@bindsolution.com</a>"
            optsMarker =
                icon: "/Images/bind-marker.png"
                title: "bindsolution"
                infoWindOpts: 
                    opts: 
                        content: contentString
            @addMarker optsMarker


    $.fn[pluginName] = (options) ->
        args = arguments;
        if options is `undefined` or typeof options is "object"
            @each ->
                $.data this, "plugin_#{pluginName}", new Plugin(this, options)  unless $.data(this, "plugin_#{pluginName}")
        else if typeof options is "string" and options[0] isnt "_" and options isnt "init"
            @each ->
                instance = $.data(this, "plugin_#{pluginName}")
                instance[options].apply instance, Array::slice.call(args, 1)  if instance instanceof Plugin and typeof instance[options] is "function"


)(jQuery, window, document) 