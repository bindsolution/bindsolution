###
    Desenvolvido pela Bind Solution
###

# Plugin
(($) ->
  $.fn.extend bindAjaxUploader: (options) ->
    defaults =
      action: "/Upload"
      multiple: false
      allowedExtensions: []
      params: {}
      onComplete: () ->
    options = $.extend(defaults, options)

    up1 = new qq.FileUploader(
      element: $(this)[0]
      allowedExtensions: options.allowedExtensions
      action: options.action
      params: options.params
      multiple: options.multiple
      onComplete: (id, fileName, responseJSON) ->
        uploaderComplete(id, fileName, responseJSON)
        if options.onComplete
          options.onComplete.call(this, responseJSON)
      messages:
        typeError: "{file} tem uma extens�o inv�lida. Apenas as extens�es {extensions} s�o permitidas.",
        sizeError: "{file} � muito grande, tamanho m�ximo permitido {sizeLimit}.",
        minSizeError: "{file} � muito pequeno, tamanho m�nimo permitido {minSizeLimit}.",
        emptyError: "{file} est� vazio, por-favor, selecione novamente os arquivos.",
        onLeave: "Os arquivos est�o sendo carregados, se voc� sair agora o upload ser� cancelado."
      template: "<div class=\"qq-uploader\">
        <div class=\"qq-upload-drop-area\">
        <span>Arraste o#{ if options.multiple then "s" else "" } arquivo#{ if options.multiple then "s" else "" }</span></div>
        <div class=\"qq-upload-button\">Selecionar arquivo#{ if options.multiple then "s" else "" }</div>
        <ul class=\"qq-upload-list\"></ul>
        </div>")
) jQuery

uploaderComplete = (id, fileName, responseJSON) -> 
  if !responseJSON.success
    spanElement = $("span.qq-upload-file:contains('" + fileName + "')").parent().children('.qq-upload-failed-text')
    if responseJSON.message
      spanElement.text spanElement.text() + " - " + responseJSON.message
    else
      spanElement.text spanElement.text() + " - Erro desconhecido"