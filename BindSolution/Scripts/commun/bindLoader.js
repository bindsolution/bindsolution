
  /*
      Desenvolvido pela Bind Solution
  */

  ;

  (function($, window, document) {
    var Plugin, defaults, pluginName;
    pluginName = "bindLoader";
    defaults = {
      autoStart: true,
      lines: 12,
      length: 0,
      width: 6,
      radius: 9,
      color: "#6AA84F",
      speed: 1,
      trail: 50,
      shadow: false,
      classText: "status-text"
    };
    Plugin = (function() {

      function Plugin(element, options) {
        this.element = element;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
      }

      Plugin.prototype.init = function() {
        this.spin = new Spinner(this.options).spin();
        if (this.options.autoStart) return this.start();
      };

      Plugin.prototype.stop = function() {
        this.spin.stop();
        return $(this.element).empty();
      };

      Plugin.prototype.start = function() {
        $(this.element).empty();
        this.element.appendChild(this.spin.el);
        if (this.options.text) this.text(this.options.text);
        return $(this.element).show();
      };

      Plugin.prototype.text = function(text) {
        return $("<div class=" + this.options.classText + ">").appendTo($(this.element)).text(text).css("line-height", "0").css("text-indent", "24px");
      };

      return Plugin;

    })();
    return $.fn[pluginName] = function(options) {
      var args;
      args = arguments;
      if (options === undefined || typeof options === "object") {
        return this.each(function() {
          if (!$.data(this, "plugin_" + pluginName)) {
            return $.data(this, "plugin_" + pluginName, new Plugin(this, options));
          }
        });
      } else if (typeof options === "string" && options[0] !== "_" && options !== "init") {
        return this.each(function() {
          var instance;
          instance = $.data(this, "plugin_" + pluginName);
          if (instance instanceof Plugin && typeof instance[options] === "function") {
            return instance[options].apply(instance, Array.prototype.slice.call(args, 1));
          }
        });
      }
    };
  })(jQuery, window, document);
