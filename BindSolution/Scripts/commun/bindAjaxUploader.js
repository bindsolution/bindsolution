(function() {

  /*
      Desenvolvido pela Bind Solution
  */

  var uploaderComplete;

  (function($) {
    return $.fn.extend({
      bindAjaxUploader: function(options) {
        var defaults, up1;
        defaults = {
          action: "/Upload",
          multiple: false,
          allowedExtensions: [],
          params: {},
          onComplete: function() {}
        };
        options = $.extend(defaults, options);
        return up1 = new qq.FileUploader({
          element: $(this)[0],
          allowedExtensions: options.allowedExtensions,
          action: options.action,
          params: options.params,
          multiple: options.multiple,
          onComplete: function(id, fileName, responseJSON) {
            uploaderComplete(id, fileName, responseJSON);
            if (options.onComplete) {
              return options.onComplete.call(this, responseJSON);
            }
          },
          messages: {
            typeError: "{file} tem uma extensão inválida. Apenas as extensões {extensions} são permitidas.",
            sizeError: "{file} é muito grande, tamanho máximo permitido {sizeLimit}.",
            minSizeError: "{file} é muito pequeno, tamanho mínimo permitido {minSizeLimit}.",
            emptyError: "{file} está vazio, por-favor, selecione novamente os arquivos.",
            onLeave: "Os arquivos estão sendo carregados, se você sair agora o upload será cancelado."
          },
          template: "<div class=\"qq-uploader\">        <div class=\"qq-upload-drop-area\">        <span>Arraste o" + (options.multiple ? "s" : "") + " arquivo" + (options.multiple ? "s" : "") + "</span></div>        <div class=\"qq-upload-button\">Selecionar arquivo" + (options.multiple ? "s" : "") + "</div>        <ul class=\"qq-upload-list\"></ul>        </div>"
        });
      }
    });
  })(jQuery);

  uploaderComplete = function(id, fileName, responseJSON) {
    var spanElement;
    if (!responseJSON.success) {
      spanElement = $("span.qq-upload-file:contains('" + fileName + "')").parent().children('.qq-upload-failed-text');
      if (responseJSON.message) {
        return spanElement.text(spanElement.text() + " - " + responseJSON.message);
      } else {
        return spanElement.text(spanElement.text() + " - Erro desconhecido");
      }
    }
  };

}).call(this);
