
  (function($) {
    $.getAntiForgeryToken = function(tokenWindow, appPath) {
      var i, inputElement, inputElements, tokenName, _results;
      tokenWindow = (tokenWindow && typeof tokenWindow === typeof window ? tokenWindow : window);
      appPath = (appPath && typeof appPath === "string" ? "_" + appPath.toString() : "");
      tokenName = "__RequestVerificationToken" + appPath;
      inputElements = tokenWindow.document.getElementsByTagName("input");
      i = 0;
      _results = [];
      while (i < inputElements.length) {
        inputElement = inputElements[i];
        if (inputElement.type === "hidden" && inputElement.name === tokenName) {
          return {
            name: tokenName,
            value: inputElement.value
          };
        }
        _results.push(i++);
      }
      return _results;
    };
    $.appendAntiForgeryToken = function(data, token) {
      if (data && typeof data !== "string") data = $.param(data);
      token = (token ? token : $.getAntiForgeryToken());
      data = (data ? data + "&" : "");
      if (token) {
        return data + encodeURIComponent(token.name) + "=" + encodeURIComponent(token.value);
      } else {
        return data;
      }
    };
    $.postAntiForgery = function(url, data, callback, type) {
      return $.post(url, $.appendAntiForgeryToken(data), callback, type);
    };
    return $.ajaxAntiForgery = function(settings) {
      var token;
      token = (settings.token ? settings.token : $.getAntiForgeryToken(settings.tokenWindow, settings.appPath));
      settings.data = $.appendAntiForgeryToken(settings.data, token);
      return $.ajax(settings);
    };
  })(jQuery);
