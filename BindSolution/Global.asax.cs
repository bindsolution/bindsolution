﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using BindSolution.Binders;
using BindSolution.Framework.Web;
using BindSolution.Mappers;
using Microsoft.Web.Optimization;
using MvcSiteMapProvider.Web;
using FrameworkBinders = BindSolution.Framework.Web.Binders;

namespace BindSolution
{
    public class MvcApplication : HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {

            filters.Add(new ElmahHandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("{*ttf}", new { ttf = @"(.*/)?BauhausRegular.ttf(/.*)?" });

            #region Project Routes

            routes.MapRoute(
                "ProjectCreate", // Route name
                "Project/Create", // URL with parameters
                new { controller = "Project", action = "Create" } // Parameter defaults
            );

            routes.MapRoute(
                "Project", // Route name
                "Project/{url}/{action}", // URL with parameters
                new { controller = "Project", action = "About" } // Parameter defaults
            );

            routes.MapRoute(
                "ProjectUpload", // Route name
                "Project/Upload/{action}/{ProjectID}", // URL with parameters
                new { controller = "Project", action = "ImageUpload" } // Parameter defaults
            );

            #endregion

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        // ReSharper disable InconsistentNaming
        protected void Application_Start()
        // ReSharper restore InconsistentNaming
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            ModelBinders.Binders.Add(typeof(WebImage), new WebImageModelBinder());
            ModelBinders.Binders.Add(typeof(double), new FrameworkBinders.DoubleModelBinder());

            XmlSiteMapController.RegisterRoutes(RouteTable.Routes);

            SetupBundle();
            SetupFluentValidation();
        }

        private static void SetupBundle()
        {
            BundleTable.Bundles.AddDefaultFileOrderings();

            BundleTable.Bundles.IgnoreList.Ignore("jquery-1*");

            var communJS = new BundleFileSetOrdering("js");
            communJS.Files.Add("prototype*.js");
            communJS.Files.Add("date-pt-BR.js");
            communJS.Files.Add("spin.js");
            communJS.Files.Add("jsrender.js");
            communJS.Files.Add("bindLoader.js");
            communJS.Files.Add("bindsolution.js");
            communJS.Files.Add("fileuploader.js");
            communJS.Files.Add("easySlider1.7.js");
            BundleTable.Bundles.FileSetOrderList.Add(communJS);

            var communCss = new BundleFileSetOrdering("css");
            communCss.Files.Add("reset.css");
            communCss.Files.Add("site.css");
            communCss.Files.Add("forms.css");
            BundleTable.Bundles.FileSetOrderList.Add(communCss);

#if DEBUG
            var jsAction = typeof(JsJoin);
            var cssAction = typeof(CssJoin);

#else
            var jsAction = typeof(JsMinify);
            var cssAction = typeof(CssMinify);
#endif

            BundleTable.Bundles.Add(new DynamicFolderBundle("js", jsAction, "*.js") { SearchSubdirectories = false });
            BundleTable.Bundles.Add(new DynamicFolderBundle("css", cssAction, "*.css") { SearchSubdirectories = false });
        }

        public static void SetupAutoMapper()
        {
            Setup.AutoMapper<BindSolutionAutoMapperProfiler>();
        }

        public static void SetupFluentValidation()
        {
            Setup.FluentValidation();
        }
    }

    internal class JsJoin : IBundleTransform
    {
        #region Implementation of IBundleTransform

        public void Process(BundleResponse bundle)
        {
            bundle.ContentType = "text/javascript";
        }

        #endregion
    }

    internal class CssJoin : IBundleTransform
    {
        #region Implementation of IBundleTransform

        public void Process(BundleResponse bundle)
        {
            bundle.ContentType = "text/css";
        }

        #endregion
    }

    class JsOrder : IBundleOrderer
    {
        private readonly IList<string> _orderFiles;

        public JsOrder(IList<string> orderFiles)
        {
            _orderFiles = orderFiles;
        }

        #region Implementation of IBundleOrderer

        public IEnumerable<FileInfo> OrderFiles(BundleContext context, IEnumerable<FileInfo> files)
        {
            return _orderFiles.Select(file => files.Where(p => p.Name.StartsWith(file))).SelectMany(filesMath => filesMath);
        }

        #endregion
    }
}