﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="RoleService.cs" company="BindSolution">
//      All rights reserved BindSolution.Service
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using System.Collections.Generic;
using System.Linq;
using BindSolution.Data;
using BindSolution.Framework.Domain;
using BindSolution.Framework.Service;

namespace BindSolution.Service
{
    public class RoleService : IRoleService
    {
        public RoleService(IRoleRepository repository)
        {
            Repository = repository;
        }

        #region Implementation of IService<Role>

        public IRepository<Role> Repository { get; private set; }

        #endregion

        #region Implementation of IRoleService

        public IEnumerable<Role> Get(string[] roleNames)
        {
            return (from r in Repository.Query()
                    where roleNames.Any(rn => rn == r.Name)
                    select r).ToList();
        }

        public Role Get(string roleName)
        {
            return (from r in Repository.Query()
                    where r.Name == roleName
                    select r).SingleOrDefault();
        }

        public bool Remove(string roleName)
        {
            var role = Get(roleName);

            if (role != null)
            {
                Repository.Remove(role);
                return Repository.Save() > 0;
            }
            return false;
        }

        public bool Exist(string roleName)
        {
            return (from r in Repository.Query()
                    where r.Name == roleName
                    select r).Any();
        }

        public void Add(string roleName)
        {
            Repository.Add(new Role
            {
                Name = roleName
            });
        }

        #endregion
    }
}