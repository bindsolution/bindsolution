﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="IEmailSend.cs" company="BindSolution">
//      All rights reserved BindSolution.Service
//  </copyright>
// -----------------------------------------------------------------------
#endregion

namespace BindSolution.Service
{
    public interface IEmailSend
    {
        void SendContactEmail(string name, string email, string message);
    }
}