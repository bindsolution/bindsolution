﻿// //-----------------------------------------------------------------------
// // <copyright file="IAddressService.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

using System;
using BindSolution.Data;
using BindSolution.Framework.Service;

namespace BindSolution.Service
{
    public interface IAddressService : IService<Address>
    {
        Address GetByProject(Project project, Guid ID);
    }
}