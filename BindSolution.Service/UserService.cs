using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BindSolution.Data;
using BindSolution.Framework;
using BindSolution.Framework.Domain;
using BindSolution.Framework.Service;

namespace BindSolution.Service
{
    public class UserService : IUserService
    {
        public UserService(IUserRepository repository)
        {
            Repository = repository;
        }

        #region Implementation of IService<User>

        public IRepository<User> Repository { get; private set; }

        #endregion

        #region Implementation of IUserService

        public void Approve(Guid gid, out User user)
        {
            user = (from u in Repository.Query()
                    where u.ID == gid
                    select u).SingleOrDefault();

            if (user == null)
                throw new InvalidUserTokenException("Token informado n�o � v�lido", gid.ToString());

            if (user.IsApproved)
                throw new EmailAlreadyConfirmedException(user.Name + ", seu email j� foi confirmado!", user);

            user.IsApproved = true;
        }

        public void ChangePassword(User user, string newPassword)
        {
            user.Password = newPassword.Sha1Hash();
        }

        public IEnumerable<User> Get(string[] emails)
        {
            return from u in Repository.Query()
                   where emails.Any(us => us == u.Email)
                   select u;
        }

        public IEnumerable<User> GetInRole(string roleName, string emailToMatch)
        {
            return from u in Repository.Query()
                   where u.Roles.Any(rl => rl.Name == roleName) && u.Email.Contains(emailToMatch)
                   select u;
        }

        public string[] GetRoles(string email)
        {
            var user = (from u in Repository.Query()
                        where u.Email == email
                        select u.Roles).SingleOrDefault();

            return user == null
                       ? null
                       : user.ToList().ConvertAll(r => r.Name).ToArray();
        }


        public string[] GetByRole(string roleName)
        {
            return (from u in Repository.Query()
                    where u.Roles.Any(r => r.Name == roleName)
                    select u.Email).ToArray();
        }

        public bool InRole(string email, string roleName)
        {
            return (from user in Repository.Query()
                    where user.Email == email && user.Roles.Any(r => r.Name == roleName)
                    select user.ID).Any();
        }

        public void RemoveFromRoles(string[] emails, string[] roleNames)
        {
            var users = from u in Repository.Query()
                        where emails.Any(uP => uP == u.Email) && u.Roles.Any(r => roleNames.Any(rp => rp == r.Name))
                        select u;

            foreach (var user in users)
                user.Roles.Remove(user.Roles.Single(r => roleNames.Any(rp => rp == r.Name)));
        }

        public bool IsValid(string email, string password, out User user)
        {
            password = password.Sha1Hash().ToLower();
            user = (from u in Repository.Query()
                    where (u.Email == email && (u.Password == password || u.Password == null))
                    select u).SingleOrDefault();
            return user != null && (user.IsApproved && !user.IsBlock);
        }

        public bool IsValid(string email, string password)
        {
            return (from u in Repository.Query()
                    where (u.Email == email
                    && u.IsApproved && !u.IsBlock)
                    && u.Password == password.Sha1Hash()
                    select u).Any();
        }

        public User Get(string email)
        {
            return (from u in Repository.Query()
                    where (u.Email == email)
                    select u).SingleOrDefault();
        }

        public void Unlock(Guid guid)
        {
            var usr = Repository.Get(guid);
            if (usr != null)
                usr.IsBlock = false;
        }

        public void Remove(string email)
        {
            Repository.Remove(Get(email));
        }

        public IEnumerable<User> Get(string emailToMatch, int pageIndex, int pageSize)
        {
            return (from u in Repository.Query()
                    where u.Email.Contains(emailToMatch)
                    orderby u.Name
                    select u).Skip(pageIndex).Take(pageSize);
        }

        public bool Exist(string email)
        {
            return (from u in Repository.Query()
                    where u.Email == email
                    select u).Any();
        }

        public void AddRoles(IEnumerable<User> users, IEnumerable<Role> roles)
        {
            foreach (var user in users)
            {
                Debug.Assert(roles != null, "roles != null");
                // ReSharper disable PossibleMultipleEnumeration
                foreach (var role in roles)
                    // ReSharper restore PossibleMultipleEnumeration
                    user.Roles.Add(role);
            }
        }

        #endregion
    }

    public class EmailAlreadyConfirmedException : ApplicationException
    {
        public EmailAlreadyConfirmedException(string message, User user)
            : base(message)
        {
            User = user;
        }

        public User User { get; private set; }
    }

    public class InvalidUserTokenException : ApplicationException
    {
        public InvalidUserTokenException(string message, string token)
            : base(message)
        {
            Token = token;
        }

        public string Token { get; private set; }
    }
}