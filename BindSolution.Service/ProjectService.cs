﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="ProjectService.cs" company="BindSolution">
//      All rights reserved BindSolution.Service
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using System;
using System.Linq;
using BindSolution.Data;
using BindSolution.Framework.Domain;

namespace BindSolution.Service
{
    public class ProjectService : IProjectService
    {
        public ProjectService(IProjectRepository repository)
        {
            Repository = repository;
        }

        #region Implementation of IService<Project>

        public IRepository<Project> Repository { get; private set; }

        public bool Exist(string name)
        {
            return Repository.Query().Any(p => p.Name == name);
        }

        public bool Exist(Guid projectID)
        {
            return Repository.Query().Any(p => p.ID == projectID);
        }

        public Address GetAddress(Project project, Guid id)
        {
            return project.Addresses.Where(p => p.ID == id).SingleOrDefault();
        }

        public Project Get(string keyword)
        {
            return Repository.Query().Where(u => u.Keyword == keyword).SingleOrDefault();
        }

        public bool KeywordExist(string keyword)
        {
            return Repository.Query().Any(p => p.Keyword == keyword);
        }

        public void AddImage(string fileName, Project project)
        {
            project.Images.Add(new ProjectImage { Name = fileName });
        }

        public string GetLogo(Guid projectID)
        {
            return (from p in Repository.Query()
                    where p.ID == projectID
                    select p.Logo).SingleOrDefault();
        }

        public Guid? GetID(string projectUrl)
        {
            return (from p in Repository.Query()
                    where p.Keyword == projectUrl
                    select p.ID).SingleOrDefault();
        }

        #endregion
    }
}