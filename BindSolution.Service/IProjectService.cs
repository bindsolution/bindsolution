﻿#region copyright
// -----------------------------------------------------------------------
//  <copyright file="IProjectService.cs" company="BindSolution">
//      All rights reserved BindSolution.Service
//  </copyright>
// -----------------------------------------------------------------------
#endregion

using System;
using BindSolution.Data;
using BindSolution.Framework.Service;

namespace BindSolution.Service
{
    public interface IProjectService : IService<Project>
    {
        bool Exist(string name);
        bool Exist(Guid projectID);
        Address GetAddress(Project project, Guid id);
        Project Get(string keyword);
        bool KeywordExist(string keyword);
        void AddImage(string fileName, Project project);
        string GetLogo(Guid projectID);
        Guid? GetID(string projectUrl);
    }
}