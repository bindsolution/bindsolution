﻿// //-----------------------------------------------------------------------
// // <copyright file="AddressService.cs" company="Bind Solution">
// //     Copyright (c) Bind Solution. All rights reserved.
// // </copyright>
// // <author>Riderman</author>
// //-----------------------------------------------------------------------

using System;
using System.Linq;
using BindSolution.Data;
using BindSolution.Framework.Domain;

namespace BindSolution.Service
{
    public class AddressService : IAddressService
    {
        #region Implementation of IService<Address>

        public IRepository<Address> Repository { get; private set; }

        public Address GetByProject(Project project, Guid ID)
        {
            return Repository.Query().Where(p => p.ID == ID).SingleOrDefault();
        }

        #endregion
    }
}