﻿using System;
using System.Linq;
using BindSolution.Controllers;
using BindSolution.Data;
using BindSolution.Service;
using BindSolution.Test.Extensions;
using BindSolution.Validators;
using BindSolution.ViewModel;
using FluentValidation.TestHelper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ProjectRepository = BindSolution.Data.Fake.ProjectRepository;

namespace BindSolution.Test
{
    [TestClass]
    public class ProjectControllerTest
    {
        private ProjectController _projectsController;
        private ProjectService _projectService;
        private ProjectValidator _projectCreateValidator;
        private Mock<IProjectImageWrap> _imageWrap;

        [TestInitialize]
        public void Initialize()
        {
            MvcApplication.SetupAutoMapper();
            MvcApplication.SetupFluentValidation();

            _imageWrap = new Mock<IProjectImageWrap>();

            _projectService = new ProjectService(new ProjectRepository());
            _projectCreateValidator = new ProjectValidator(_projectService, _imageWrap.Object);
            //_projectsController = new ProjectController(_projectService, _imageWrap.Object);
            _projectsController.SetFakeControllerContext();
        }

        #region Controller

        [TestMethod]
        public void WhenCreatingAProjectAdressesShouldBeEmpty()
        {
            //arrange
            var model = new ProjectViewModel
                            {
                                Name = "AMSD Arquitetura"
                                ,
                                Description = "Escritório de designer de interiores"
                                ,
                                WebSite = "amsdarquitetura.com.br"
                            };

            //act
            _projectsController.Create(model);

            //asserts
            var projectsResult = _projectService.Repository.Query().Where(p => p.Name == model.Name).Single();
            Assert.IsTrue(projectsResult.Addresses == null || projectsResult.Addresses.Count == 0);
        }

        [TestMethod]
        public void WhenCreatingAProjectShouldBeInTheRepository()
        {
            //arrange
            var model = new ProjectViewModel
            {
                Name = "AMSD Arquitetura"
                ,
                Description = "Escritório de designer de interiores"
                ,
                WebSite = "amsdarquitetura.com.br"
            };


            //act
            _projectsController.Create(model);

            //asserts
            var projectsResult = _projectService.Repository.Query().Where(p => p.Name == model.Name).Single();
            Assert.IsNotNull(projectsResult);
        }

        [TestMethod]
        public void WhenCreatingAProjectValidateThatThereIsAnotherProjectWithSameName()
        {
            //arrange
            var model = new ProjectViewModel
            {
                Name = "AMSD Arquitetura"
                ,
                Description = "Escritório de designer de interiores"
                ,
                WebSite = "amsdarquitetura.com.br"
            };

            var model2 = new ProjectViewModel
            {
                Name = "AMSD Arquitetura"
                ,
                Description = "Escritório de designer de interiores 2"
                ,
                WebSite = "amsdarquitetura2.com.br"
            };


            //act
            _projectsController.Create(model);

            //assert
            var projectsResult = _projectService.Repository.Query().Where(p => p.Name == model.Name).ToList();
            Assert.AreEqual(1, projectsResult.Count);
            _projectCreateValidator.ShouldHaveValidationErrorFor(p => p.Name, model2);


        }

        [TestMethod]
        public void WhenCreatingAProjectWithTwoAddressesShouldBeInTheRepository()
        {
            //arrange
            var model = new ProjectViewModel
            {
                Name = "AMSD Arquitetura"
                ,
                Description = "Escritório de designer de interiores"
                ,
                WebSite = "amsdarquitetura.com.br"
            };

            //act
            _projectsController.Create(model);

            //asserts
            var projectsResult = _projectService.Repository.Query().Where(p => p.Name == model.Name).Single();
            Assert.IsNotNull(projectsResult);
            Assert.AreEqual(2, projectsResult.Addresses.Count);
            CollectionAssert.AllItemsAreUnique(projectsResult.Addresses.ToList());
        }

        [TestMethod]
        public void WhenCreatingAProjectImagesCreateMethodMustBeInvoked()
        {
            //arrange
            var model = new ProjectViewModel
            {
                Name = "AMSD Arquitetura"
                ,
                Description = "Escritório de designer de interiores"
                ,
                WebSite = "amsdarquitetura.com.br"
            };

            //act
            _projectsController.Create(model);

            //asserts
            var projectsResult = _projectService.Repository.Query().Where(p => p.Name == model.Name).Single();
            Assert.IsNotNull(projectsResult);
            _imageWrap.Verify(foo => foo.Save(It.IsAny<Guid>()), "When doing operation Create, the ´Create´ should be called always");
            _imageWrap.Verify(foo => foo.Get(It.IsAny<Guid>()), Times.Never());
        }

        [TestMethod]
        public void WhenEditAProjectGetImagesMethodMustBeInvoked()
        {
            //arrange
            _projectService.Repository.PopuleProjects(3);
            var project = _projectService.Repository.Query().First();

            //arrange
            var model = new ProjectViewModel
            {
                Name = project.Name + "(edit)"
                ,
                Description = project.Description
                ,
                WebSite = project.WebSite
            };

            //act
            _projectsController.Edit(model, project);

            //asserts
            var projectsResult = _projectService.Repository.Query().Where(p => p.Name == model.Name).Single();
            Assert.IsNotNull(projectsResult);
            _imageWrap.Verify(foo => foo.Get(It.IsAny<Guid>()), "When doing operation ´Edit´, the ´GetImages´ should be called always");
            _imageWrap.Verify(foo => foo.Save(It.IsAny<Guid>()), Times.Never());
        }


        #endregion

        #region Validations

        [TestMethod]
        public void ValidateWhenCreatingAProjectProjectNameIsRequired()
        {
            //arrange
            var model = new ProjectViewModel
            {
                Description = "Escritório de designer de interiores"
                ,
                WebSite = "amsdarquitetura.com.br"
            };

            //act

            //asserts
            _projectCreateValidator.ShouldHaveValidationErrorFor(p => p.Name, model);

        }

        #endregion
    }

}
