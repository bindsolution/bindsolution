﻿    using BindSolution.Data;
using BindSolution.Framework.Domain;

namespace BindSolution.Test.Extensions
{
    public static class IRepositoryExtensions
    {
        public static void PopuleProjects(this IRepository<Project> projectRepository, int total)
        {
            for (int i = 0; i < total; i++)
            {
                projectRepository.Add(new Project
                                          {
                                              Name = string.Format("Project ({0})", i),
                                              WebSite = string.Format("www.myproject{0}.com.br", i),
                                              Description = string.Format("This is my #{0} project! =)", i)
                                          });
            }
        }

    }
}
